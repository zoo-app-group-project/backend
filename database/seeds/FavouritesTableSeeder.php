<?php

declare(strict_types=1);

use Carbon\Carbon;
use Faker\Provider\Lorem;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Zoo\Http\Rest\GeolocalizerClient;
use Zoo\Models\User;
use Zoo\Models\ZooReview;

/**
 * Class FavouritesTableSeeder
 */
class FavouritesTableSeeder extends Seeder
{
    /** @var array */
    protected $zooIds = [];

    /**
     * ZooReviewsTableSeeder constructor.
     * @param GeolocalizerClient $client
     */
    public function __construct(GeolocalizerClient $client)
    {
        $zoos = $client->get('/points/zoo')->get('points');

        $this->zooIds = collect($zoos)->pluck('id');
    }

    /**
     * @throws Exception
     * @return void
     */
    public function run(): void
    {
        $favourites = [];

        $now = Carbon::now()->toDateTimeString();

        foreach ($this->zooIds as $zooId) {
            foreach (User::all() as $user) {
                if(random_int(1, 5) == 1){
                    $favourites[] = [
                        'id' => Str::uuid(),
                        'user_id' => $user->id,
                        'zoo_id' => $zooId,
                        'created_at' => $now,
                        'updated_at' => $now,
                    ];
                }
            }
        }

        foreach (collect($favourites)->chunk(500) as $favourites) {
            ZooReview::query()->insert($favourites->toArray());
        }
    }
}
