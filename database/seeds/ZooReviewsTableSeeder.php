<?php

declare(strict_types=1);

use Carbon\Carbon;
use Faker\Provider\Lorem;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Zoo\Http\Rest\GeolocalizerClient;
use Zoo\Models\User;
use Zoo\Models\ZooReview;

/**
 * Class ZooReviewsTableSeeder
 */
class ZooReviewsTableSeeder extends Seeder
{
    /** @var array */
    protected $zooIds = [];

    /**
     * ZooReviewsTableSeeder constructor.
     * @param GeolocalizerClient $client
     */
    public function __construct(GeolocalizerClient $client)
    {
        $zoos = $client->get('/points/zoo')->get('points');


        $this->zooIds = collect($zoos)->pluck('id');
    }

    /**
     * @throws Exception
     */
    public function run(): void
    {
        $reviews = [];

        $now = Carbon::now()->toDateTimeString();

        foreach ($this->zooIds as $zooId) {
            foreach (User::all() as $user) {
                $reviews[] = [
                    'id' => Str::uuid(),
                    'user_id' => $user->id,
                    'zoo_id' => $zooId,
                    'rating' => random_int(1, 5),
                    'content' => Lorem::text(),
                    'created_at' => $now,
                    'updated_at' => $now,
                ];
            }
        }

        foreach (collect($reviews)->chunk(500) as $reviews) {
            ZooReview::query()->insert($reviews->toArray());
        }
    }
}
