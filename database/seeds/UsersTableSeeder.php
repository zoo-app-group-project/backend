<?php

declare(strict_types=1);

use Illuminate\Database\Seeder;
use Zoo\Models\User;

/**
 * Class UsersTableSeeder
 */
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        factory(User::class, 50)->create();
    }
}
