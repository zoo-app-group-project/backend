<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateFavouritesTable
 */
class CreateFavouritesTable extends Migration
{
    /**
     * @return void
     */
    public function up(): void
    {
        Schema::create('favourites', function (Blueprint $table): void {
            $table->uuid('id')->primary();

            $table->uuid('user_id');
            $table->foreign('user_id')->references('id')->on('users');

            $table->string('zoo_id');

            $table->unique(['user_id', 'zoo_id']);

            $table->timestamps();
        });
    }

    /**
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('favourites');
    }
}
