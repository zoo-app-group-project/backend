<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateFriendsTable
 */
class CreateFriendsTable extends Migration
{
    /**
     * @return void
     */
    public function up(): void
    {
        Schema::create('friends', function (Blueprint $table): void {
            $table->uuid('id')->primary();

            $table->uuid('user_id');
            $table->foreign('user_id')->references('id')->on('users');

            $table->uuid('friend_id');
            $table->foreign('friend_id')->references('id')->on('users');

            $table->unique(['user_id', 'friend_id']);

            $table->timestamps();
        });
    }

    /**
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('friends');
    }
}
