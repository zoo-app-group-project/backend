<?php

declare(strict_types=1);

return [
    'defaults' => [
        'guard' => 'api',
        'passwords' => 'users',
    ],
    'guards' => [
        'api' => [
            'driver' => 'passport',
            'provider' => 'users',
        ],
    ],
    'providers' => [
        'users' => [
            'driver' => 'eloquent',
            'model' => Zoo\Models\User::class,
        ],
    ],
    'passwords' => [
        'users' => [
            'provider' => 'users',
            'table' => 'password_resets',
            'expire' => 60,
        ],
    ],
    'oauth' => [
        'grant_type' => env('OAUTH_GRANT_TYPE', 'password'),
        'client_id' => env('OAUTH_CLIENT_ID', 2),
        'client_secret' => env('OAUTH_CLIENT_SECRET', ''),
    ]
];
