<?php

declare(strict_types=1);

return [
    'host' => env('GEOLOCALIZER_HOST'),
    'access_token' => env('GEOLOCALIZER_ACCESS_TOKEN'),
];
