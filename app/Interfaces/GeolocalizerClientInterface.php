<?php

declare(strict_types=1);

namespace Zoo\Interfaces;

use Zoo\Http\Rest\JsonResponse;

/**
 * Interface GeolocalizerClientInterface
 * @package Zoo\Interfaces
 */
interface GeolocalizerClientInterface
{
    /**
     * @param $method
     * @param string $path
     * @param array $options
     * @return JsonResponse
     */
    public function request($method, $path = "", array $options = []): JsonResponse;

    /**
     * @return string
     */
    public function getHost(): string;

    /**
     * @return array
     */
    public function getHeaders(): array;
}
