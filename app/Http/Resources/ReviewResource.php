<?php

declare(strict_types=1);

namespace Zoo\Http\Resources;

class ReviewResource extends Resource
{
    /**
     * @return array
     */
    public function map(): array
    {
        return [
            'id' => $this->resource['id'],
            'user' => $this->resource['user'],
            'content' => $this->resource['content'],
            'rating' => $this->resource['rating'],
            'createdAt' => $this->resource['created_at']->toDateTimeString(),
            'diffForHumans' => $this->resource['created_at']->diffForHumans(),
        ];
    }
}
