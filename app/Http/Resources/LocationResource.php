<?php

declare(strict_types=1);

namespace Zoo\Http\Resources;

/**
 * Class LocationResource
 * @package Zoo\Http\Resources
 */
class LocationResource extends Resource
{
    /**
     * @return array
     */
    public function map(): array
    {
        return [
            'longitude' => $this->resource[0],
            'latitude' => $this->resource[1],
        ];
    }
}
