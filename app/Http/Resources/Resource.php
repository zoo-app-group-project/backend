<?php

declare(strict_types=1);

namespace Zoo\Http\Resources;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Collection;

/**
 * Class Resource
 * @package Zoo\Http\Resources
 */
abstract class Resource
{
    /** @var Arrayable */
    protected $resource;

    /**
     * Resource constructor.
     * @param Arrayable $resource
     */
    public function __construct(Arrayable $resource)
    {
        $this->resource = $resource;
    }

    /**
     * @param array $resource
     * @return static
     */
    public static function fromArray(array $resource): self
    {
        $class = get_called_class();
        return new $class(new ResourceArray($resource));
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $data = [];

        if ($this->resource instanceof Collection) {
            foreach ($this->resource as $resource) {
                $this->resource = $resource;
                $data[] = $this->map();
            }
        } else {
            $data = $this->map();
        }

        return $data;
    }

    /**
     * @return array
     */
    abstract public function map(): array;
}
