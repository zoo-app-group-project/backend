<?php

declare(strict_types=1);

namespace Zoo\Http\Resources;

/**
 * Class AddressResource
 * @package Zoo\Http\Resources
 */
class AddressResource extends Resource
{
    /**
     * @return array
     */
    public function map(): array
    {
        return [
            'full' => $this->resource['full'] ?? '',
            'city' => $this->resource['city'] ?? '',
            'state' => $this->resource['state'] ?? '',
            'country' => $this->resource['country'] ?? '',
            'countryCode' => $this->resource['country_code'] ?? '',
        ];
    }
}
