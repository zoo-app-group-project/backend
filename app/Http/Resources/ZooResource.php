<?php

declare(strict_types=1);

namespace Zoo\Http\Resources;

/**
 * Class ZooResource
 * @package Zoo\Http\Resources
 */
class ZooResource extends Resource
{
    /**
     * @return array
     */
    public function map(): array
    {
        return [
            'id' => $this->resource['id'],
            'name' => $this->resource['name'],
            'location' => LocationResource::fromArray($this->resource['location'])->toArray(),
            'description' => $this->resource['payload']['description'],
            'logo' => $this->resource['payload']['logo'],
            'wikipediaLink' => $this->resource['payload']['wikipediaLink'],
            'address' => AddressResource::fromArray($this->resource['payload']['address'])->toArray(),
            'rating' => $this->resource['rating'] ?? null,
            'myReview' => $this->resource['myReview'] ?? null,
            'recentReviews' => $this->resource['recentReviews'] ?? [],
        ];
    }
}
