<?php

declare(strict_types=1);

namespace Zoo\Http\Resources;

/**
 * Class CityResource
 * @package Zoo\Http\Resources
 */
class CityResource extends Resource
{
    /**
     * @return array
     */
    public function map(): array
    {
        return [
            'name' => $this->resource['name'],
            'fullName' => $this->resource['fullName'],
            'location' => $this->resource['location'],
        ];
    }
}
