<?php

declare(strict_types=1);

namespace Zoo\Http\Rest;

use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\Response;
use Zoo\Exceptions\GeolocalizerClientException;
use Zoo\Exceptions\ZooNotFoundException;
use Zoo\Interfaces\GeolocalizerClientInterface;

/**
 * Class GeolocalizerClient
 * @package Zoo\Http\Rest
 */
class GeolocalizerClient extends RestClient implements GeolocalizerClientInterface
{
    /**
     * @param $method
     * @param string $path
     * @param array $options
     * @return JsonResponse
     * @throws GeolocalizerClientException
     * @throws ZooNotFoundException
     */
    public function request($method, $path = "", array $options = []): JsonResponse
    {
        try {
            $response = $this->requestAsync($method, $this->buildUri($path), $this->buildOptions())->wait();
        } catch (GuzzleException $exception) {
            if ($exception->getCode() === Response::HTTP_NOT_FOUND) {
                throw new ZooNotFoundException();
            }

            throw new GeolocalizerClientException();
        }

        return JsonResponse::fromJsonString($response->getBody()->getContents());
    }

    /**
     * @return string
     */
    public function getHost(): string
    {
        return (string)config('geolocalizer.host');
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Authorization' => 'Bearer ' . config('geolocalizer.access_token'),
        ];
    }
}
