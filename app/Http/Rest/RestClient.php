<?php

declare(strict_types=1);

namespace Zoo\Http\Rest;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\RequestOptions;
use Illuminate\Http\Request;
use Psr\Http\Message\UriInterface;

/**
 * Class RestClient
 * @package Zoo\Http\Rest
 * @method JsonResponse post(string|UriInterface $uri, array $options = [])
 * @method JsonResponse get(string|UriInterface $uri, array $options = [])
 * @method JsonResponse put(string|UriInterface $uri, array $options = [])
 * @method JsonResponse patch(string|UriInterface $uri, array $options = [])
 * @method JsonResponse delete(string|UriInterface $uri, array $options = [])
 */
class RestClient extends GuzzleClient
{
    /** @var array */
    protected $data = [];
    /** @var array */
    protected $headers = [];
    /** @var Request */
    protected $request;

    /**
     * RestClient constructor.
     * @param Request $request
     * @param array $config
     */
    public function __construct(Request $request, array $config = [])
    {
        parent::__construct($config);
        $this->request = $request;
    }

    /**
     * @param $method
     * @param string $path
     * @param array $options
     * @return JsonResponse
     */
    public function request($method, $path = "", array $options = []): JsonResponse
    {
        $response = $this->requestAsync($method, $this->buildUri($path), $this->buildOptions())->wait();

        return JsonResponse::fromJsonString($response->getBody()->getContents());
    }

    /**
     * @param array $data
     * @return RestClient
     */
    public function setData(array $data): RestClient
    {
        $this->data = $data;
        return $this;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $headers
     * @return RestClient
     */
    public function setHeaders(array $headers): RestClient
    {
        $this->headers = $headers;
        return $this;
    }

    /**
     * @param string $key
     * @param string $value
     * @return RestClient
     */
    public function setHeader(string $key, string $value): RestClient
    {
        $this->headers[$key] = $value;
        return $this;
    }

    /**
     * @param string $accessToken
     * @return RestClient
     */
    public function setAccessToken(string $accessToken): RestClient
    {
        $this->headers['Authorization'] = "Bearer $accessToken";
        return $this;
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return array_merge($this->getDefaultHeaders(), $this->headers);
    }

    /**
     * @return string
     */
    public function getHost(): string
    {
        return '';
    }

    /**
     * @param string $path
     * @return string
     */
    protected function buildUri(string $path)
    {
        return $this->getHost() . $path;
    }

    /**
     * @return array
     */
    protected function getDefaultHeaders(): array
    {
        return [
            "Accept" => "application/json",
            "Content-Type" => "application/json",
        ];
    }

    /**
     * @return array
     */
    protected function buildOptions(): array
    {
        return [
            RequestOptions::SYNCHRONOUS => true,
            'headers' => $this->getHeaders(),
            'json' => $this->getData(),
        ];
    }
}
