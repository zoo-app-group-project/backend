<?php

declare(strict_types=1);

namespace Zoo\Http\Rest;

use Illuminate\Http\JsonResponse as BaseResponse;

/**
 * Class JsonResponse
 * @package Zoo\Http\Rest
 */
class JsonResponse extends BaseResponse
{
    /** @var array */
    private $body = [];
    /** @var array */
    private $errors = [];

    /**
     * @param null $data
     * @param int $status
     * @param array $headers
     * @return JsonResponse
     */
    public static function fromJsonString($data = null, $status = 200, $headers = []): JsonResponse
    {
        $data = json_decode($data, true);
        $response = new self($data, $status, $headers);

        $response->setBody($data["body"] ?? $data);
        $response->setErrors($data["errors"] ?? []);

        return $response;
    }

    /**
     * @param array $data
     * @return JsonResponse
     */
    public static function fromArray(array $data): JsonResponse
    {
        return static::fromJsonString(json_encode($data));
    }

    /**
     * @return array
     */
    public function getBody(): array
    {
        return $this->body;
    }

    /**
     * @param array $body
     * @return JsonResponse
     */
    public function setBody(array $body): JsonResponse
    {
        $this->body = $body;
        return $this;
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function get(string $key)
    {
        return $this->body[$key];
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @param array $errors
     * @return JsonResponse
     */
    public function setErrors(array $errors): JsonResponse
    {
        $this->errors = $errors;
        return $this;
    }

    /**
     * @return int
     */
    public function countErrors(): int
    {
        return count($this->getErrors());
    }

    /**
     * @return bool
     */
    public function hasErrors(): bool
    {
        return $this->countErrors() !== 0;
    }
}
