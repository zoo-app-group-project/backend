<?php

declare(strict_types=1);

namespace Zoo\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;

/**
 * Class Controller
 * @package Zoo\Http\Controllers
 */
class Controller extends BaseController
{
}
