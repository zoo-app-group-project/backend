<?php

declare(strict_types=1);

namespace Zoo\Http\GraphQL\Mutations;

use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use Zoo\Models\User;
use Zoo\Services\UserCreator;

/**
 * Class CreateUser
 * @package Zoo\Http\GraphQL\Mutations
 */
class CreateUser
{
    /** @var UserCreator */
    protected $userCreator;

    /**
     * CreateUser constructor.
     * @param UserCreator $userCreator
     */
    public function __construct(UserCreator $userCreator)
    {
        $this->userCreator = $userCreator;
    }

    /**
     * @param $rootValue
     * @param array $args
     * @param GraphQLContext $context
     * @param ResolveInfo $resolveInfo
     * @return User
     */
    public function resolve($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo): User
    {
        return $this->userCreator->create($args);
    }
}
