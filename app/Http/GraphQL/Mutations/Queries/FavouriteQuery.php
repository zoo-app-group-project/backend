<?php

declare(strict_types=1);

namespace Zoo\Http\GraphQL\Mutations\Queries;

use Zoo\Interfaces\GeolocalizerClientInterface as GeolocalizerClient;
use Zoo\Services\Managers\FavouriteManager;

/**
 * Class FavouriteQuery
 * @package Zoo\Http\GraphQL\Mutations\Queries
 */
abstract class FavouriteQuery
{
    /** @var FavouriteManager */
    protected $favouriteManager;
    /** @var GeolocalizerClient */
    protected $client;

    /**
     * PostReview constructor.
     * @param FavouriteManager $favouriteManager
     */
    public function __construct(FavouriteManager $favouriteManager, GeolocalizerClient $client)
    {
        $this->favouriteManager = $favouriteManager;
        $this->client = $client;
    }
}
