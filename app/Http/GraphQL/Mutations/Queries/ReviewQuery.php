<?php

declare(strict_types=1);

namespace Zoo\Http\GraphQL\Mutations\Queries;

use Zoo\Services\Managers\ReviewManager;

/**
 * Class ReviewQuery
 * @package Zoo\Http\GraphQL\Mutations\Queries
 */
abstract class ReviewQuery
{
    /** @var ReviewManager */
    protected $reviewManager;

    /**
     * PostReview constructor.
     * @param ReviewManager $reviewManager
     */
    public function __construct(ReviewManager $reviewManager)
    {
        $this->reviewManager = $reviewManager;
    }
}
