<?php

declare(strict_types=1);

namespace Zoo\Http\GraphQL\Mutations\Queries;

use Zoo\Interfaces\GeolocalizerClientInterface as GeolocalizerClient;
use Zoo\Services\Managers\FriendManager;

/**
 * Class FriendQuery
 * @package Zoo\Http\GraphQL\Mutations\Queries
 */
abstract class FriendQuery
{
    /** @var FriendManager */
    protected $friendManager;
    /** @var GeolocalizerClient */
    protected $client;

    /**
     * PostReview constructor.
     * @param FriendManager $friendManager
     * @param GeolocalizerClient $client
     */
    public function __construct(FriendManager $friendManager, GeolocalizerClient $client)
    {
        $this->friendManager = $friendManager;
        $this->client = $client;
    }
}
