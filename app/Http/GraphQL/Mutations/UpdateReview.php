<?php

declare(strict_types=1);

namespace Zoo\Http\GraphQL\Mutations;

use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use Zoo\Exceptions\ReviewNotFoundException;
use Zoo\Exceptions\UnauthorizedException;
use Zoo\Http\GraphQL\Mutations\Queries\ReviewQuery;
use Zoo\Models\ZooReview;

/**
 * Class UpdateReview
 * @package Zoo\Http\GraphQL\Mutations
 */
class UpdateReview extends ReviewQuery
{
    /**
     * @param $rootValue
     * @param array $args
     * @param GraphQLContext $context
     * @param ResolveInfo $resolveInfo
     * @return ZooReview
     * @throws ReviewNotFoundException
     * @throws UnauthorizedException
     */
    public function resolve($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo): ZooReview
    {
        $args['userId'] = $context->user()->id;
        return $this->reviewManager->update($args);
    }
}
