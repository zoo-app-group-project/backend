<?php

declare(strict_types=1);

namespace Zoo\Http\GraphQL\Mutations;

use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use Zoo\Http\GraphQL\Mutations\Queries\FriendQuery;
use Zoo\Exceptions\UserNotFoundException;

/**
 * Class RemoveFriend
 * @package Zoo\Http\GraphQL\Mutations
 */
class RemoveFriend extends FriendQuery
{
    /**
     * @param $rootValue
     * @param array $args
     * @param GraphQLContext $context
     * @param ResolveInfo $resolveInfo
     * @return bool
     * @throws UserNotFoundException
     */
    public function resolve($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo): bool
    {
        $this->friendManager->remove($args, $context->user()->id);

        return true;
    }
}
