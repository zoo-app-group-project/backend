<?php

declare(strict_types=1);

namespace Zoo\Http\GraphQL\Mutations;

use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use Zoo\Exceptions\ZooAlreadyReviewed;
use Zoo\Exceptions\ZooNotFoundException;
use Zoo\Http\GraphQL\Mutations\Queries\ReviewQuery;
use Zoo\Models\ZooReview;

/**
 * Class PostReview
 * @package Zoo\Http\GraphQL\Mutations
 */
class PostReview extends ReviewQuery
{
    /**
     * @param $rootValue
     * @param array $args
     * @param GraphQLContext $context
     * @param ResolveInfo $resolveInfo
     * @return ZooReview
     * @throws ZooAlreadyReviewed
     * @throws ZooNotFoundException
     */
    public function resolve($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo): ZooReview
    {
        $args['userId'] = $context->user()->id;
        return $this->reviewManager->create($args);
    }
}
