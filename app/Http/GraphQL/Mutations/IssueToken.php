<?php

declare(strict_types=1);

namespace Zoo\Http\GraphQL\Mutations;

use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use Zoo\Exceptions\InvalidCredentialsException;
use Zoo\Services\OAuthProxy;

/**
 * Class IssueToken
 * @package Zoo\Http\GraphQL\Mutations
 */
class IssueToken
{
    /** @var OAuthProxy */
    protected $oAuthProxy;

    /**
     * IssueToken constructor.
     * @param OAuthProxy $oAuthProxy
     */
    public function __construct(OAuthProxy $oAuthProxy)
    {
        $this->oAuthProxy = $oAuthProxy;
    }

    /**
     * @param $rootValue
     * @param array $args
     * @param GraphQLContext $context
     * @param ResolveInfo $resolveInfo
     * @return string
     * @throws InvalidCredentialsException
     */
    public function resolve($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo): string
    {
        return $this->oAuthProxy->issueTokenByCredentials($args['email'], $args['password']);
    }
}
