<?php

declare(strict_types=1);

namespace Zoo\Http\GraphQL\Mutations;

use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use Zoo\Http\GraphQL\Mutations\Queries\FriendQuery;
use Zoo\Exceptions\CannotBefriendSelfException;
use Zoo\Exceptions\UserNotFoundException;
use Zoo\Exceptions\FriendAlreadyAddedException;
use Zoo\Models\User;

/**
 * Class AddFavourite
 * @package Zoo\Http\GraphQL\Mutations
 */
class AddFriend extends FriendQuery
{
    /**
     * @param $rootValue
     * @param array $args
     * @param GraphQLContext $context
     * @param ResolveInfo $resolveInfo
     * @return User
     * @throws CannotBefriendSelfException
     * @throws FriendAlreadyAddedException
     * @throws UserNotFoundException
     */
    public function resolve($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo): User
    {
        return $this->friendManager->create($args, $context->user()->id);
    }
}
