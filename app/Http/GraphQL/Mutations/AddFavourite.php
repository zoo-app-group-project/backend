<?php

declare(strict_types=1);

namespace Zoo\Http\GraphQL\Mutations;

use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use Zoo\Exceptions\ZooAlreadyFavouritedException;
use Zoo\Exceptions\ZooNotFoundException;
use Zoo\Http\GraphQL\Mutations\Queries\FavouriteQuery;
use Zoo\Models\Favourite;

/**
 * Class AddFavourite
 * @package Zoo\Http\GraphQL\Mutations
 */
class AddFavourite extends FavouriteQuery
{
    /**
     * @param $rootValue
     * @param array $args
     * @param GraphQLContext $context
     * @param ResolveInfo $resolveInfo
     * @return Favourite
     * @throws ZooAlreadyFavouritedException
     * @throws ZooNotFoundException
     */
    public function resolve($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo): Favourite
    {
        $args['userId'] = $context->user()->id;
        return $this->favouriteManager->create($args);
    }
}
