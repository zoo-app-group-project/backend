<?php

declare(strict_types=1);

namespace Zoo\Http\GraphQL\Mutations;

use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use Zoo\Exceptions\FavouriteNotFoundException;
use Zoo\Exceptions\ReviewNotFoundException;
use Zoo\Exceptions\UnauthorizedException;
use Zoo\Http\GraphQL\Mutations\Queries\FavouriteQuery;

/**
 * Class RemoveFavourite
 * @package Zoo\Http\GraphQL\Mutations
 */
class RemoveFavourite extends FavouriteQuery
{
    /**
     * @param $rootValue
     * @param array $args
     * @param GraphQLContext $context
     * @param ResolveInfo $resolveInfo
     * @return bool
     * @throws FavouriteNotFoundException
     * @throws UnauthorizedException
     */
    public function resolve($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo): bool
    {
        $args['userId'] = $context->user()->id;

        $this->favouriteManager->delete($args);

        return true;
    }
}
