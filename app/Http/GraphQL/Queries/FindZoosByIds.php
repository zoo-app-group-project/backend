<?php

declare(strict_types=1);

namespace Zoo\Http\GraphQL\Queries;

/**
 * Class FindZoosByIds
 * @package Zoo\Http\GraphQL\Queries
 */
class FindZoosByIds extends ZooQuery
{
    /**
     * @param array $args
     */
    public function query(array $args): void
    {
        $this->client->setData([
            'select' => $args['ids'],
        ]);

        $response = $this->client->get('/points/zoo');

        $this->zoos = collect($response->get('points'));
    }
}
