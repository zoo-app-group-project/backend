<?php

declare(strict_types=1);

namespace Zoo\Http\GraphQL\Queries;

/**
 * Class FindNearbyZoos
 * @package Zoo\Http\GraphQL\Queries
 */
class FindNearbyZoos extends ZooQuery
{
    /**
     * @param array $args
     */
    public function query(array $args): void
    {
        $this->client->setData([
            'longitude' => $args['longitude'],
            'latitude' => $args['latitude'],
            'kilometers' => $args['range'],
        ]);

        $response = $this->client->get('/points/zoo/search/range');

        $this->zoos = collect($response->get('results'));
    }
}
