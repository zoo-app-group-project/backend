<?php

declare(strict_types=1);

namespace Zoo\Http\GraphQL\Queries;

use GraphQL\Type\Definition\ResolveInfo;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use Zoo\Http\Resources\ZooResource;
use Zoo\Interfaces\GeolocalizerClientInterface as GeolocalizerClient;
use Zoo\Models\User;
use Zoo\Models\ZooReview;

/**
 * Class ZooQuery
 * @package Zoo\Http\GraphQL\Queries
 */
abstract class ZooQuery
{
    /** @var GeolocalizerClient */
    protected $client;
    /** @var Collection */
    protected $zoos;

    /**
     * ZooQuery constructor.
     * @param GeolocalizerClient $client
     */
    public function __construct(GeolocalizerClient $client)
    {
        $this->client = $client;
    }

    /**
     * @param $rootValue
     * @param array $args
     * @param GraphQLContext $context
     * @param ResolveInfo $resolveInfo
     * @return array
     */
    public function resolve($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo): array
    {
        $this->query($args);

        $selectionSet = $resolveInfo->getFieldSelection();

        if (!empty($selectionSet['rating'])) {
            $this->withRating();
        }

        if (!empty($selectionSet['recentReviews'])) {
            $this->withRecentReviews();
        }

        if (!empty($selectionSet['myReview']) && $context->user()) {
            $this->withMyReview($context->user());
        }

        return $this->getResults();
    }

    /**
     * @param array $args
     */
    abstract public function query(array $args): void;


    /**
     * @return array
     */
    protected function getResults(): array
    {
        return (new ZooResource($this->zoos))->toArray();
    }

    /**
     * @return ZooQuery
     */
    protected function withRating(): self
    {
        $ratings = ZooReview::query()
            ->whereIn('zoo_id', $this->getIds())
            ->groupBy('zoo_id')
            ->select('zoo_id', DB::raw('AVG(rating) as value'))
            ->get();

        $this->zoos = $this->zoos->map(function (array $zoo) use ($ratings): array {
            $rating = $ratings->where('zoo_id', $zoo['id'])->first();
            $zoo['rating'] = $rating['value'] ? round($rating['value'], 2) : 0;
            return $zoo;
        });

        return $this;
    }

    /**
     * @return $this
     */
    protected function withRecentReviews(): self
    {
        $reviews = ZooReview::query()
            ->with('user')
            ->whereIn('zoo_id', $this->getIds())
            ->orderByDesc('created_at')
            ->limit(10)
            ->get();

        $this->zoos = $this->zoos->map(function (array $zoo) use ($reviews): array {
            $zooReviews = $reviews->where('zoo_id', $zoo['id']);

            $zoo['recentReviews'] = $zooReviews->isNotEmpty() ? $zooReviews : [];
            return $zoo;
        });

        return $this;
    }

    /**
     * @param User $user
     * @return $this
     */
    protected function withMyReview(User $user): self
    {
        $reviews = ZooReview::query()
            ->with('user')
            ->where('user_id', $user->id)
            ->get();

        $this->zoos = $this->zoos->map(function (array $zoo) use ($reviews): array {
            $review = $reviews->where('zoo_id', $zoo['id'])->first();
            $zoo['myReview'] = $review ?? null;
            return $zoo;
        });

        return $this;
    }

    /**
     * @return array
     */
    protected function getIds(): array
    {
        return $this->zoos
            ->pluck('id')
            ->toArray();
    }
}
