<?php

declare(strict_types=1);

namespace Zoo\Http\GraphQL\Queries;

use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use Zoo\Http\Resources\CityResource;
use Zoo\Interfaces\GeolocalizerClientInterface as GeolocalizerClient;

/**
 * Class FindCities
 * @package Zoo\Http\GraphQL\Queries
 */
class FindCities
{
    /** @var GeolocalizerClient */
    protected $client;

    /**
     * FindCity constructor.
     * @param GeolocalizerClient $client
     */
    public function __construct(GeolocalizerClient $client)
    {
        $this->client = $client;
    }

    /**
     * @param $rootValue
     * @param array $args
     * @param GraphQLContext $context
     * @param ResolveInfo $resolveInfo
     * @return array
     */
    public function resolve($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo): array
    {
        $this->client->setData($args);

        $response = $this->client->get('/geocoding/autocomplete');

        $cities = new CityResource(collect($response->get('results')));

        return $cities->toArray();
    }
}
