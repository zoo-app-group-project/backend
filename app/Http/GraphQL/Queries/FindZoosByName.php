<?php

declare(strict_types=1);

namespace Zoo\Http\GraphQL\Queries;

/**
 * Class FindZoosByName
 * @package Zoo\Http\GraphQL\Queries
 */
class FindZoosByName extends ZooQuery
{
    /**
     * @param array $args
     */
    public function query(array $args): void
    {
        $this->client->setData([
            'name' => $args['query'],
        ]);

        $response = $this->client->get('/points/zoo/search/name');

        $this->zoos = collect($response->get('results'));
    }
}
