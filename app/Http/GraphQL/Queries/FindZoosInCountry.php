<?php

declare(strict_types=1);

namespace Zoo\Http\GraphQL\Queries;

/**
 * Class FindZoosInCountry
 * @package Zoo\Http\GraphQL\Queries
 */
class FindZoosInCountry extends ZooQuery
{
    /**
     * @param array $args
     */
    public function query(array $args): void
    {
        $this->client->setData([
            'country' => $args['countryCode'],
        ]);

        $response = $this->client->get('/country/points/zoo');

        $this->zoos = collect($response->get('points'));
    }
}
