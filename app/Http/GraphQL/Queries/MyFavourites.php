<?php

declare(strict_types=1);

namespace Zoo\Http\GraphQL\Queries;

use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use Zoo\Http\GraphQL\Mutations\Queries\FavouriteQuery;

/**
 * Class MyFavourites
 * @package Zoo\Http\GraphQL\Queries
 */
class MyFavourites extends FavouriteQuery
{

    /**
     * @param $rootValue
     * @param array $args
     * @param GraphQLContext $context
     * @param ResolveInfo $resolveInfo
     * @return array
     */
    public function resolve($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo): array
    {
        return $this->favouriteManager->getFavourites($context->user()->id);
    }
}
