<?php

declare(strict_types=1);

namespace Zoo\Http\GraphQL\Queries;

/**
 * Class GetAllZoos
 * @package Zoo\Http\GraphQL\Queries
 */
class GetAllZoos extends ZooQuery
{
    /**
     * @param array $args
     */
    public function query(array $args): void
    {
        $response = $this->client->get('/points/zoo');

        $this->zoos = collect($response->get('points'));
    }
}
