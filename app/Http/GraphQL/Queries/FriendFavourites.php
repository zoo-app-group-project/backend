<?php

declare(strict_types=1);

namespace Zoo\Http\GraphQL\Queries;

use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use Zoo\Http\GraphQL\Mutations\Queries\FriendQuery;
use Zoo\Exceptions\CannotGetStrangerFavouritesException;
use Zoo\Exceptions\UserNotFoundException;

/**
 * Class FriendFavourites
 * @package Zoo\Http\GraphQL\Queries
 */
class FriendFavourites extends FriendQuery
{


    /**
     * @param $rootValue
     * @param array $args
     * @param GraphQLContext $context
     * @param ResolveInfo $resolveInfo
     * @return array
     * @throws CannotGetStrangerFavouritesException
     * @throws UserNotFoundException
     */
    public function resolve($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo): array
    {
        return $this->friendManager->getFriendFavourites($args, $context->user()->id);
    }
}
