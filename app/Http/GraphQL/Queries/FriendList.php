<?php

declare(strict_types=1);

namespace Zoo\Http\GraphQL\Queries;

use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use Zoo\Http\GraphQL\Mutations\Queries\FriendQuery;

/**
 * Class FriendList
 * @package Zoo\Http\GraphQL\Queries
 */
class FriendList extends FriendQuery
{

    /**
     * @param $rootValue
     * @param array $args
     * @param GraphQLContext $context
     * @param ResolveInfo $resolveInfo
     * @return array
     */
    public function resolve($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo): array
    {
        return $this->friendManager->getFriendList($context->user()->id);
    }
}
