<?php

declare(strict_types=1);

namespace Zoo\Http\GraphQL\Queries;

use Zoo\Http\Resources\ZooResource;

/**
 * Class Zoo
 * @package Zoo\Http\GraphQL\Queries
 */
class Zoo extends ZooQuery
{
    /**
     * @param array $args
     */
    public function query(array $args): void
    {
        $id = $args['id'];

        $response = $this->client->get("/points/zoo/$id");

        $this->zoos = collect([$response->get('point')]);
    }

    /**
     * @return array
     */
    protected function getResults(): array
    {
        $resource = (new ZooResource($this->zoos))->toArray();
        return head($resource);
    }
}
