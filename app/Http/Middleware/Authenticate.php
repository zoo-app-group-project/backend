<?php

declare(strict_types=1);

namespace Zoo\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as BaseAuthenticate;
use Illuminate\Http\Request;
use Zoo\Exceptions\UnauthenticatedException;

/**
 * Class Authenticate
 * @package Zoo\Http\Middleware
 */
class Authenticate extends BaseAuthenticate
{
    /**
     * @param Request $request
     * @param array $guards
     * @throws UnauthenticatedException
     */
    protected function unauthenticated($request, array $guards): void
    {
        throw new UnauthenticatedException();
    }
}
