<?php

declare(strict_types=1);

namespace Zoo\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

/**
 * Class Kernel
 * @package Zoo\Http
 */
class Kernel extends HttpKernel
{
    /** @var array */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\ValidatePostSize::class,
        \Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull::class,
    ];
    /** @var array */
    protected $middlewareGroups = [
        'api' => [
            'throttle:60,1',
            'bindings',
        ],
    ];

    /** @var array */
    protected $routeMiddleware = [
        'auth' => \Zoo\Http\Middleware\Authenticate::class,
        'bindings' => \Illuminate\Routing\Middleware\SubstituteBindings::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
    ];

    /**
     * @var array
     */
    protected $middlewarePriority = [
        \Illuminate\Routing\Middleware\ThrottleRequests::class,
        \Illuminate\Auth\Middleware\Authorize::class,
    ];
}
