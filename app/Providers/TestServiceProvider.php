<?php

declare(strict_types=1);

namespace Zoo\Providers;

use Illuminate\Support\ServiceProvider;
use Zoo\Interfaces\GeolocalizerClientInterface;
use Zoo\Tests\Mocks\Geolocation\GeolocalizerClient;

/**
 * Class TestServiceProvider
 * @package Zoo\Providers
 */
class TestServiceProvider extends ServiceProvider
{
    /**
     * @return void
     */
    public function register(): void
    {
    }

    /**
     * @return void
     */
    public function boot(): void
    {
        $this->app->bind(GeolocalizerClientInterface::class, GeolocalizerClient::class);
    }
}
