<?php

declare(strict_types=1);

namespace Zoo\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Laravel\Passport\Passport;
use Laravel\Passport\RouteRegistrar;

/**
 * Class AuthServiceProvider
 * @package Zoo\Providers
 */
class AuthServiceProvider extends ServiceProvider
{
    /**
     * @var array
     */
    protected $policies = [

    ];

    /**
     * @return void
     */
    public function boot(): void
    {
        $this->registerPolicies();

        Passport::routes(function (RouteRegistrar $router): void {
            $router->forAccessTokens();
        });
    }
}
