<?php

declare(strict_types=1);

namespace Zoo\Providers;

use Illuminate\Support\ServiceProvider;
use Zoo\Http\Rest\GeolocalizerClient;
use Zoo\Interfaces\GeolocalizerClientInterface;

/**
 * Class AppServiceProvider
 * @package Zoo\Providers
 */
class AppServiceProvider extends ServiceProvider
{
    /**
     * @return void
     */
    public function register(): void
    {
    }

    /**
     * @return void
     */
    public function boot(): void
    {
        $this->app->bind(GeolocalizerClientInterface::class, GeolocalizerClient::class);
    }
}
