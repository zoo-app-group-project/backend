<?php

declare(strict_types=1);

namespace Zoo\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

/**
 * Class Kernel
 * @package Zoo\Console
 */
class Kernel extends ConsoleKernel
{
    /** @var array */
    protected $commands = [];

    /**
     * @param Schedule $schedule
     */
    protected function schedule(Schedule $schedule): void
    {
    }

    /**
     * @return void
     */
    protected function commands(): void
    {
    }
}
