<?php

declare(strict_types=1);

namespace Zoo\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Zoo\Models\Traits\UsesUuid;

/**
 * Class ZooReview
 * @package Zoo\Models
 * @property string $id
 * @property string $zoo_id
 * @property array $zoo
 * @property int $user_id
 * @property User $user
 * @property string $content
 * @property int $rating
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class ZooReview extends Model
{
    use UsesUuid;

    /** @var array */
    protected $fillable = [
        'user_id', 'zoo_id', 'rating', 'content',
    ];

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
