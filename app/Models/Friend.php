<?php

declare(strict_types=1);

namespace Zoo\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Zoo\Models\Traits\UsesUuid;

/**
 * Class Friend
 * @package Zoo\Models
 * @property User $user
 * @property string $user_id
 * @property User $friend
 * @property string $friend_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class Friend extends Model
{
    use UsesUuid;

    /** @var array */
    protected $fillable = [
        'user_id', 'friend_id'
    ];

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * @return BelongsTo
     */
    public function friend(): BelongsTo
    {
        return $this->belongsTo(User::class, 'friend_id');
    }
}
