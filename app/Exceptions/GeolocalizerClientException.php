<?php

declare(strict_types=1);

namespace Zoo\Exceptions;

/**
 * Class GeolocalizerClientException
 * @package Zoo\Exceptions
 */
class GeolocalizerClientException extends GraphQLException
{
    /** @var string */
    protected $message = 'Internal request to geolocation service failed. Please try again later.';

    /**
     * @return string
     */
    public function getCategory(): string
    {
        return 'geolocation';
    }
}
