<?php

declare(strict_types=1);

namespace Zoo\Exceptions;

/**
 * Class UserNotFoundException
 * @package Zoo\Exceptions
 */
class UserNotFoundException extends GraphQLException
{
    /** @var string */
    protected $message = 'User not found.';

    /**
     * @return string
     */
    public function getCategory(): string
    {
        return 'auth';
    }
}
