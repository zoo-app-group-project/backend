<?php

declare(strict_types=1);

namespace Zoo\Exceptions;

/**
 * Class InvalidCredentialsException
 * @package Zoo\Exceptions
 */
class InvalidCredentialsException extends GraphQLException
{
    /** @var string */
    protected $message = 'Invalid credentials.';

    /**
     * @return string
     */
    public function getCategory(): string
    {
        return 'validation';
    }
}
