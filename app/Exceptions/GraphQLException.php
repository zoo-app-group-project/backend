<?php

declare(strict_types=1);

namespace Zoo\Exceptions;

use Exception;
use Nuwave\Lighthouse\Exceptions\RendersErrorsExtensions;

/**
 * Class GraphQLException
 * @package Zoo\Exceptions
 */
class GraphQLException extends Exception implements RendersErrorsExtensions
{
    /**
     * @return string
     */
    public function getCategory(): string
    {
        return 'internal';
    }

    /**
     * @return bool
     */
    public function isClientSafe(): bool
    {
        return true;
    }

    /**
     * @return array
     */
    public function extensionsContent(): array
    {
        return [];
    }
}
