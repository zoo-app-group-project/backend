<?php

declare(strict_types=1);

namespace Zoo\Exceptions;

/**
 * Class ZooNotFoundException
 * @package Zoo\Exceptions
 */
class ZooNotFoundException extends GraphQLException
{
    /** @var string */
    protected $message = 'Zoo not found.';

    /**
     * @return string
     */
    public function getCategory(): string
    {
        return 'zoo';
    }
}
