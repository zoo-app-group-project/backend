<?php

declare(strict_types=1);

namespace Zoo\Exceptions;

/**
 * Class FriendAlreadyAddedException
 * @package Zoo\Exceptions
 */
class FriendAlreadyAddedException extends GraphQLException
{
    /** @var string */
    protected $message = 'Friend already added.';

    /**
     * @return string
     */
    public function getCategory(): string
    {
        return 'auth';
    }
}
