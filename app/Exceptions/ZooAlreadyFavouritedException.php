<?php

declare(strict_types=1);

namespace Zoo\Exceptions;

/**
 * Class ZooAlreadyFavouritedException
 * @package Zoo\Exceptions
 */
class ZooAlreadyFavouritedException extends GraphQLException
{
    /** @var string */
    protected $message = 'This zoo is already in favourite list.';

    /**
     * @return string
     */
    public function getCategory(): string
    {
        return 'zoo';
    }
}
