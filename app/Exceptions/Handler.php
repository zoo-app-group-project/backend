<?php

declare(strict_types=1);

namespace Zoo\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

/**
 * Class Handler
 * @package Zoo\Exceptions
 */
class Handler extends ExceptionHandler
{
    /**
     * @var array
     */
    protected $dontReport = [

    ];

    /**
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * @param Exception $exception
     * @throws Exception
     */
    public function report(Exception $exception): void
    {
        Log::error($exception->getMessage(), ['file' => $exception->getFile(), 'stack' => $exception->getTraceAsString()]);
    }

    /**
     * @param Request $request
     * @param Exception $exception
     * @return Response|\Symfony\Component\HttpFoundation\Response
     */
    public function render($request, Exception $exception)
    {
        return parent::render($request, $exception);
    }
}
