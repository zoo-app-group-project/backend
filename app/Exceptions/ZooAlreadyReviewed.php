<?php

declare(strict_types=1);

namespace Zoo\Exceptions;

/**
 * Class ZooAlreadyReviewed
 * @package Zoo\Exceptions
 */
class ZooAlreadyReviewed extends GraphQLException
{
    /** @var string */
    protected $message = 'This zoo is already reviewed.';

    /**
     * @return string
     */
    public function getCategory(): string
    {
        return 'zoo';
    }
}
