<?php

declare(strict_types=1);

namespace Zoo\Exceptions;

/**
 * Class CannotGetStrangerFavouritesException
 * @package Zoo\Exceptions
 */
class CannotGetStrangerFavouritesException extends GraphQLException
{
    /** @var string */
    protected $message = 'Cannot access favourites of stranger.';

    /**
     * @return string
     */
    public function getCategory(): string
    {
        return 'favourite';
    }
}
