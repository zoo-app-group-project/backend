<?php

declare(strict_types=1);

namespace Zoo\Exceptions;

/**
 * Class CannotBefriendSelfException
 * @package Zoo\Exceptions
 */
class CannotBefriendSelfException extends GraphQLException
{
    /** @var string */
    protected $message = 'Cannot add yourself as friend.';

    /**
     * @return string
     */
    public function getCategory(): string
    {
        return 'auth';
    }
}
