<?php

declare(strict_types=1);

namespace Zoo\Exceptions;

/**
 * Class ReviewNotFoundException
 * @package Zoo\Exceptions
 */
class ReviewNotFoundException extends GraphQLException
{
    /** @var string */
    protected $message = 'Review not found.';

    /**
     * @return string
     */
    public function getCategory(): string
    {
        return 'review';
    }
}
