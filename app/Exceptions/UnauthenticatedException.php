<?php

declare(strict_types=1);

namespace Zoo\Exceptions;

/**
 * Class UnauthenticatedException
 * @package Zoo\Exceptions
 */
class UnauthenticatedException extends GraphQLException
{
    /** @var string */
    protected $message = 'Unauthenticated.';

    /**
     * @return string
     */
    public function getCategory(): string
    {
        return 'auth';
    }
}
