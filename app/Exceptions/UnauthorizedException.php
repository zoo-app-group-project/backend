<?php

declare(strict_types=1);

namespace Zoo\Exceptions;

/**
 * Class UnauthorizedException
 * @package Zoo\Exceptions
 */
class UnauthorizedException extends GraphQLException
{
    /** @var string */
    protected $message = 'Unauthorized.';

    /**
     * @return string
     */
    public function getCategory(): string
    {
        return 'auth';
    }
}
