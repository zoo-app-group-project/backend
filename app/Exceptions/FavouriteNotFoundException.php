<?php

declare(strict_types=1);

namespace Zoo\Exceptions;

/**
 * Class FavouriteNotFoundException
 * @package Zoo\Exceptions
 */
class FavouriteNotFoundException extends GraphQLException
{
    /** @var string */
    protected $message = 'Favourite not found.';

    /**
     * @return string
     */
    public function getCategory(): string
    {
        return 'favourite';
    }
}
