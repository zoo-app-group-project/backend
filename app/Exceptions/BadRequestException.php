<?php

declare(strict_types=1);

namespace Zoo\Exceptions;

/**
 * Class BadRequestExceptionException
 * @package Zoo\Exceptions
 */
class BadRequestException extends GraphQLException
{
    /** @var string */
    protected $message = 'Bad request.';

    /**
     * @return string
     */
    public function getCategory(): string
    {
        return 'validation';
    }
}
