<?php

declare(strict_types=1);

namespace Zoo\Tests\Contexts;

use Behat\Behat\Context\Context;
use Zoo\Tests\Traits\AuthenticatesUsers;
use Zoo\Tests\Traits\InitializesApplication;
use Zoo\Tests\Traits\MakesGraphQLRequests;
use Zoo\Tests\Traits\ManipulatesFavourites;
use Zoo\Tests\Traits\ManipulatesFriends;
use Zoo\Tests\Traits\ManipulatesReviews;

/**
 * Class FeatureContext
 * @package Zoo\Tests\Contexts
 */
class FeatureContext implements Context
{
    use InitializesApplication;
    use AuthenticatesUsers;
    use MakesGraphQLRequests;
    use ManipulatesReviews;
    use ManipulatesFavourites;
    use ManipulatesFriends;
}
