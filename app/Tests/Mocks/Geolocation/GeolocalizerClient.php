<?php

declare(strict_types=1);

namespace Zoo\Tests\Mocks\Geolocation;

use Zoo\Exceptions\ZooNotFoundException;
use Zoo\Http\Rest\JsonResponse;
use Zoo\Http\Rest\RestClient;
use Zoo\Interfaces\GeolocalizerClientInterface;

/**
 * Class GeolocalizerClient
 * @package Zoo\Tests\Mocks\Geolocation
 */
class GeolocalizerClient extends RestClient implements GeolocalizerClientInterface
{
    /**
     * @param $method
     * @param string $path
     * @param array $options
     * @return JsonResponse
     * @throws ZooNotFoundException
     */
    public function request($method, $path = "", array $options = []): JsonResponse
    {
        if ($path === "/points/zoo/non-existing-zoo-uuid") {
            throw new ZooNotFoundException();
        }

        $fileName = $this->getRouteMapping()[$path];

        $json = file_get_contents(__DIR__ . "/Responses/$fileName");

        return JsonResponse::fromJsonString($json);
    }

    /**
     * @return string
     */
    public function getHost(): string
    {
        return '';
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return [];
    }

    /**
     * @return array
     */
    protected function getRouteMapping(): array
    {
        return [
            '/points/zoo' => 'getAllPoints.json',
            '/points/zoo/5df14a534f15997edd650ce7' => 'getPointById.json',
            '/points/zoo/search/name' => 'searchPointsByName.json',
            '/points/zoo/search/range' => 'searchPointsInRange.json',
            '/country/points/zoo' => 'getPointsInCountry.json',
            '/geocoding/autocomplete' => 'findCities.json'
        ];
    }
}
