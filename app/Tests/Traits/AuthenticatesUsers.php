<?php

declare(strict_types=1);

namespace Zoo\Tests\Traits;

use Illuminate\Foundation\Application;
use Laravel\Passport\Passport;
use Zoo\Models\User;
use Zoo\Services\UserCreator;

/**
 * Trait AuthenticatesUsers
 * @package Zoo\Tests\Traits
 * @property Application $app
 */
trait AuthenticatesUsers
{
    /**
     * @Given user with :email email, :name name and :password password exists
     * @param string $email
     * @param string $name
     * @param string $password
     */
    public function userWithEmailNameAndPasswordExists(string $email, string $name, string $password): void
    {
        $user = $this->app->make(UserCreator::class)->create([
            'email' => $email,
            'name' => $name,
            'password' => $password
        ]);
    }

    /**
     * @Given user with :email email is authenticated
     * @param $email
     */
    public function userWithEmailIsAuthenticated(string $email): void
    {
        Passport::actingAs(User::whereEmail($email)->first());
    }

    /**
     * @Given graphQL query request for :email user
     * @param $email
     */
    public function graphqlQueryRequestForUser(string $email): void
    {
        $userId = User::query()->where('email', $email)->firstOrFail()->id;

        $this->request = "
            query {
              user(id: \"$userId\") {
                  id
                  name
                  email
              }
            }
        ";
    }

    /**
     * @param string $email
     * @return User
     */
    protected function getUserByEmail(string $email): User
    {
        return User::query()
            ->where('email', $email)
            ->firstOrFail();
    }
}
