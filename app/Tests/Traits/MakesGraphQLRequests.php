<?php

declare(strict_types=1);

namespace Zoo\Tests\Traits;

use Behat\Behat\Tester\Exception\PendingException;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Illuminate\Foundation\Application;
use Illuminate\Foundation\Testing\Concerns\MakesHttpRequests;
use Illuminate\Foundation\Testing\TestResponse;
use Illuminate\Support\Arr;
use Nuwave\Lighthouse\Testing\MakesGraphQLRequests as BaseMakesGraphQLRequests;
use PHPUnit\Framework\Assert;

/**
 * Trait MakesGraphQLRequests
 * @package Zoo\Tests\Traits
 * @property Application $app
 */
trait MakesGraphQLRequests
{
    use BaseMakesGraphQLRequests;
    use MakesHttpRequests;

    /** @var string */
    private $request;
    /** @var TestResponse */
    private $response;

    /**
     * @Given graphQL request:
     * @param PyStringNode $request
     */
    public function graphqlRequest(PyStringNode $request): void
    {
        $this->request = $request->getRaw();
    }

    /**
     * @When request is sent
     */
    public function requestIsSent(): void
    {
        $this->response = $this->postGraphQL([
            'query' => $this->request
        ]);
    }

    /**
     * @Then response should exist
     */
    public function responseShouldExist(): void
    {
        Assert::assertNotNull($this->response);
    }

    /**
     * @Then response should not have any errors
     */
    public function responseShouldNotHaveAnyErrors(): void
    {
        $this->response->assertDontSee("errors");
    }

    /**
     * @Then response should have:
     * @param TableNode $body
     */
    public function responseShouldHave(TableNode $body): void
    {
        $response = $this->getResponseArray();

        foreach ($body as $element) {
            Assert::assertTrue(Arr::has($response, $element["key"]));

            if (!empty($element["value"])) {
                $flattedResponse = Arr::dot($response);
                Assert::assertEquals($flattedResponse[$element["key"]], $element["value"]);
            }
        }
    }

    /**
     * @Given response should not have:
     * @param TableNode $body
     */
    public function responseShouldNotHave(TableNode $body): void
    {
        foreach ($body as $element) {
            Assert::assertFalse(Arr::has($this->getResponseArray(), $element["key"]));
        }
    }

    /**
     * @Given :key should count :count elements
     * @param string $key
     * @param int $count
     */
    public function shouldCountElements(string $key, int $count): void
    {
        $value = $this->response->json($key);
        Assert::assertIsArray($value);
        Assert::assertCount($count, $value);
    }

    /**
     * @return array
     */
    protected function getResponseArray(): array
    {
        return json_decode($this->response->getContent(), true);
    }

    /**
     * @Given :key should be set but null
     * @param string $key
     */
    public function shouldBeSetButNull(string $key): void
    {
        $response = Arr::dot($this->getResponseArray());
        Assert::assertTrue(Arr::has($response, $key));
        Assert::assertNull($response[$key]);
    }
}
