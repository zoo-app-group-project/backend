<?php

declare(strict_types=1);

namespace Zoo\Tests\Traits;

use PHPUnit\Framework\Assert;
use Zoo\Models\Friend;

/**
 * Trait ManipulatesFriends
 * @package Zoo\Tests\Traits
 */
trait ManipulatesFriends
{
    /**
     * @Given user :email should have :friendsCount friends
     * @param string $email
     * @param int $friendsCount
     */
    public function userShouldHaveFriends(string $email, int $friendsCount): void
    {
        $user = $this->getUserByEmail($email);
        $count = Friend::where('user_id', $user->id)->count();

        Assert::assertEquals($friendsCount, $count);
    }

    /**
     * @Given user :email has friend with email :friendEmail
     * @param string $email
     * @param string $friendEmail
     */
    public function userHasFriendWithEmail(string $email, string $friendEmail): void
    {
        $user = $this->getUserByEmail($email);
        $friend = $this->getUserByEmail($friendEmail);

        Friend::query()->create([
            'user_id' => $user->id,
            'friend_id' => $friend->id,
        ]);
    }
}
