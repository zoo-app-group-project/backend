<?php

declare(strict_types=1);

namespace Zoo\Tests\Traits;

use PHPUnit\Framework\Assert;
use Zoo\Models\Favourite;

/**
 * Trait ManipulatesFavourites
 * @package Zoo\Tests\Traits
 */
trait ManipulatesFavourites
{
    /**
     * @Given graphQL request to add zoo with id :zooId to favourites
     * @param string $zooId
     */
    public function graphqlRequestToAddZooWithIdToFavourites(string $zooId): void
    {
        $this->request = "
            mutation {
                addFavourite(zooId: \"$zooId\") {
                    id
                    user {
                        id
                        name
                        email
                    }
                    zooId
                    createdAt
                }
            }
        ";
    }

    /**
     * @Given zoo with id :id is already added to favourites by user :email
     * @param string $id
     * @param string $email
     */
    public function zooWithIdIsAlreadyAddedToFavouritesByUser(string $id, string $email): void
    {
        $userId = $this->getUserByEmail($email)->id;

        Favourite::query()->create([
            'zoo_id' => $id,
            'user_id' => $userId,
        ]);
    }

    /**
     * @Given user :email should have :favCount favourite zoos
     * @param string $email
     * @param int $favCount
     */
    public function userShouldHaveFavouriteZoos(string $email, int $favCount): void
    {
        $userId = $this->getUserByEmail($email)->id;

        $count = Favourite::query()
            ->where('user_id', $userId)
            ->count();

        Assert::assertEquals($count, $favCount);
    }
}
