<?php

declare(strict_types=1);

namespace Zoo\Tests\Traits;

use PHPUnit\Framework\Assert;
use Zoo\Models\ZooReview;

/**
 * Trait ManipulatesReviews
 * @package Zoo\Tests\Traits
 */
trait ManipulatesReviews
{
    /**
     * @Then average rating of zoo with id :id should be :value
     * @param string $id
     * @param float $value
     */
    public function averageRatingOfZooWithIdShouldBe(string $id, float $value): void
    {
        $rating = ZooReview::query()->where('zoo_id', $id)->average('rating');
        Assert::assertEquals($rating, $value);
    }

    /**
     * @Then review of zoo with id :id posted by :email with :rating rating and :content content should exist in the database
     * @param string $id
     * @param string $email
     * @param float $rating
     * @param string $content
     */
    public function reviewOfZooWithIdPostedByWithRatingAndContentShouldExistInTheDatabase(string $id, string $email, float $rating, string $content): void
    {
        $userId = $this->getUserByEmail($email)->id;

        $review = ZooReview::query()
            ->where('zoo_id', $id)
            ->where('user_id', $userId)
            ->where('rating', $rating)
            ->where('content', $content);

        Assert::assertTrue($review->exists());
    }

    /**
     * @Given zoo with id :id is already reviewed by :email with :rating rating and :content content
     * @param string $id
     * @param string $email
     * @param float $rating
     * @param string $content
     */
    public function zooWithIdIsAlreadyReviewedByWithRatingAndContent(string $id, string $email, float $rating, string $content): void
    {
        $userId = $this->getUserByEmail($email)->id;

        ZooReview::query()->create([
            'zoo_id' => $id,
            'user_id' => $userId,
            'rating' => $rating,
            'content' => $content,
        ]);
    }

    /**
     * @Given graphQL request to fetch review posted by :email of :zooId zoo
     * @param string $email
     * @param string $zooId
     */
    public function graphqlRequestToFetchReviewPostedByEmailOfZooIdZoo(string $email, string $zooId): void
    {
        $userId = $this->getUserByEmail($email)->id;

        $reviewId = ZooReview::query()
            ->where('user_id', $userId)
            ->where('zoo_id', $zooId)
            ->firstOrFail()
            ->id;

        $this->request = "
            query {
                review(id: \"$reviewId\") {
                    id
                    user {
                        id
                        name
                        email
                    }
                    zooId
                    rating
                    content
                    createdAt
                }
            }
        ";
    }

    /**
     * @Given graphQL request to update review posted by :email of :zooId zoo
     * @param string $email
     * @param string $zooId
     */
    public function graphqlRequestToUpdateReviewPostedByOfZoo(string $email, string $zooId): void
    {
        $userId = $this->getUserByEmail($email)->id;

        $reviewId = ZooReview::query()
            ->where('user_id', $userId)
            ->where('zoo_id', $zooId)
            ->firstOrFail()
            ->id;

        $this->request = "
            mutation {
                updateReview(
                    id: \"$reviewId\"
                    rating: 1
                    content: \"Very bad.\"
                ) {
                    id
                    user {
                        id
                        name
                        email
                    }
                    zooId
                    rating
                    content
                    createdAt
                }
            }
        ";
    }

    /**
     * @Given graphQL request to delete review posted by :email of :zooId zoo
     * @param string $email
     * @param string $zooId
     */
    public function graphqlRequestToDeleteReviewPostedByOfZoo(string $email, string $zooId): void
    {
        $userId = $this->getUserByEmail($email)->id;

        $reviewId = ZooReview::query()
            ->where('user_id', $userId)
            ->where('zoo_id', $zooId)
            ->firstOrFail()
            ->id;

        $this->request = "
            mutation {
                deleteReview(id: \"$reviewId\")
            }
        ";
    }

    /**
     * @Given review of zoo with id :id posted by :email with :rating rating and :content content should not exist in the database
     * @param string $id
     * @param string $email
     * @param float $rating
     * @param string $content
     */
    public function reviewOfZooWithIdPostedByWithRatingAndContentShouldNotExistInTheDatabase(string $id, string $email, float $rating, string $content): void
    {
        $userId = $this->getUserByEmail($email)->id;

        $review = ZooReview::query()
            ->where('zoo_id', $id)
            ->where('user_id', $userId)
            ->where('rating', $rating)
            ->where('content', $content);

        Assert::assertFalse($review->exists());
    }
}
