<?php

declare(strict_types=1);

namespace Zoo\Services;

use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Zoo\Exceptions\InvalidCredentialsException;

/**
 * Class OAuthProxy
 * @package Zoo\Services
 */
class OAuthProxy
{
    /** @var Application */
    private $application;

    /**
     * OAuthProxy constructor.
     * @param Application $application
     */
    public function __construct(Application $application)
    {
        $this->application = $application;
    }

    /**
     * @param string $email
     * @param string $password
     * @return string
     * @throws InvalidCredentialsException
     */
    public function issueTokenByCredentials(string $email, string $password): string
    {
        $parameters = [
            'grant_type' => config('auth.oauth.grant_type'),
            'client_id' => config('auth.oauth.client_id'),
            'client_secret' => config('auth.oauth.client_secret'),
            'username' => $email,
            'password' => $password
        ];

        $request = Request::create('/oauth/token', Request::METHOD_POST, $parameters);

        $response = $this->application->handle($request);

        if ($response->getStatusCode() === Response::HTTP_BAD_REQUEST) {
            throw new InvalidCredentialsException();
        }

        return json_decode($response->getContent(), true)['access_token'];
    }
}
