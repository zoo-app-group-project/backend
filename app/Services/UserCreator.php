<?php

declare(strict_types=1);

namespace Zoo\Services;

use Illuminate\Support\Facades\Hash;
use Zoo\Models\User;

/**
 * Class UserCreator
 * @package Zoo\Services
 */
class UserCreator
{
    /**
     * @param array $data
     * @return User
     */
    public function create(array $data): User
    {
        $user = new User();
        $user->email = $data['email'];
        $user->name = $data['name'];
        $user->password = Hash::make($data['password']);
        $user->save();

        return $user;
    }
}
