<?php

declare(strict_types=1);

namespace Zoo\Services\Managers;

use Illuminate\Database\Eloquent\Builder;
use Zoo\Exceptions\CannotBefriendSelfException;
use Zoo\Exceptions\CannotGetStrangerFavouritesException;
use Zoo\Exceptions\FavouriteNotFoundException;
use Zoo\Exceptions\FriendAlreadyAddedException;
use Zoo\Exceptions\UnauthorizedException;
use Zoo\Exceptions\UserNotFoundException;
use Zoo\Models\Favourite;
use Zoo\Models\Friend;
use Zoo\Models\User;
use Zoo\Services\ZooRepository;

/**
 * Class FriendManager
 * @package Zoo\Services\Managers
 */
class FriendManager
{
    /** @var ZooRepository */
    protected $zooRepository;

    /**
     * ReviewManager constructor.
     * @param ZooRepository $zooRepository
     */
    public function __construct(ZooRepository $zooRepository)
    {
        $this->zooRepository = $zooRepository;
    }

    /**
     * @param array $data
     * @param string $authId
     * @return User
     * @throws CannotBefriendSelfException
     * @throws FriendAlreadyAddedException
     * @throws UserNotFoundException
     */
    public function create(array $data, string $authId): User
    {
        $user = $this->getUserByEmail($data['email']);

        if (!$user) {
            throw new UserNotFoundException();
        }

        if ($this->isAlreadyFriend($user, $authId)) {
            throw new FriendAlreadyAddedException();
        }

        if ($authId == $user->id) {
            throw new CannotBefriendSelfException();
        }

        Friend::create([
            'user_id' => $authId,
            'friend_id' => $user->id,
        ]);

        return $user;
    }

    /**
     * @param array $data
     * @param string $authId
     * @throws UserNotFoundException
     */
    public function remove(array $data, string $authId): void
    {
        $user = $this->getUserByEmail($data['email']);

        if (!$user) {
            throw new UserNotFoundException();
        }

        Friend::where([
            'user_id'=> $authId,
            'friend_id'=> $user->id,
        ])->delete();
    }

    /**
     * @param $userId
     * @return array
     */
    public function getFriendList($userId): array
    {
        $auth = User::with('friends')->where('id', $userId)->first();

        return $auth->friends->map(function($friend) {
            return $friend->friend;
        })->all();
    }

    /**
     * @param array $args
     * @param string $authId
     * @return array
     * @throws CannotGetStrangerFavouritesException
     * @throws UserNotFoundException
     */
    public function getFriendFavourites(array $args, string $authId): array
    {
        $auth = $this->getUserById($authId);
        $user = $this->getUserByEmail($args['email']);

        $friend = $auth->friends()->where('friend_id', $user->id)->first();

        if (!$friend) {
            throw new CannotGetStrangerFavouritesException();
        }

        return Favourite::with('user')->where('user_id', $user->id)->get()->toArray();
    }

    /**
     * @param string $email
     * @return User
     * @throws UserNotFoundException
     */
    protected function getUserByEmail(string $email): User
    {
        $user = User::with('friends')
            ->where('email', $email)
            ->first();

        if(!$user){
            throw new UserNotFoundException();
        }

        return $user;
    }

    /**
     * @param string $userId
     * @return User
     * @throws UserNotFoundException
     */
    protected function getUserById(string $userId): User
    {
        $user = User::with('friends')
            ->where('id', $userId)
            ->firstOrFail();

        if(!$user){
            throw new UserNotFoundException();
        }

        return $user;
    }

    /**
     * @param User $user
     * @param string $friendId
     * @return bool
     */
    private function isAlreadyFriend(User $user, string $friendId): bool
    {
        $friend = Friend::where('friend_id', $friendId)->first();

        return $friend != null;
    }



}
