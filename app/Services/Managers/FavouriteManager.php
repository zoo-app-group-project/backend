<?php

declare(strict_types=1);

namespace Zoo\Services\Managers;

use Illuminate\Database\Eloquent\Builder;
use Zoo\Exceptions\FavouriteNotFoundException;
use Zoo\Exceptions\UnauthorizedException;
use Zoo\Exceptions\ZooAlreadyFavouritedException;
use Zoo\Exceptions\ZooNotFoundException;
use Zoo\Models\Favourite;
use Zoo\Services\ZooRepository;

/**
 * Class FavouriteManager
 * @package Zoo\Services\Managers
 */
class FavouriteManager
{
    /** @var ZooRepository */
    protected $zooRepository;

    /**
     * ReviewManager constructor.
     * @param ZooRepository $zooRepository
     */
    public function __construct(ZooRepository $zooRepository)
    {
        $this->zooRepository = $zooRepository;
    }

    /**
     * @param array $data
     * @return Favourite
     * @throws ZooAlreadyFavouritedException
     * @throws ZooNotFoundException
     */
    public function create(array $data): Favourite
    {
        if (!$this->zooRepository->exists($data['zooId'])) {
            throw new ZooNotFoundException();
        }

        if ($this->zooRepository->isFavouritedByUser($data['zooId'], $data['userId'])) {
            throw new ZooAlreadyFavouritedException();
        }

        return Favourite::query()->create([
            'user_id' => $data['userId'],
            'zoo_id' => $data['zooId'],
        ]);
    }

    /**
     * @param array $data
     * @throws FavouriteNotFoundException
     * @throws UnauthorizedException
     */
    public function delete(array $data): void
    {
        $this->getFavouriteByQuery($data['zooId'], $data['userId'])->delete();
    }

    /**
     * @param string $userId
     * @return array
     */
    public function getFavourites(string $userId): array
    {
        return $favourites = Favourite::query()->with('user')->where('user_id', $userId)->get()->toArray();
    }

    /**
     * @param string $id
     * @param string $userId
     * @return Builder
     * @throws FavouriteNotFoundException
     * @throws UnauthorizedException
     */
    protected function getFavouriteByQuery(string $id, string $userId): Builder
    {
        $favourite = Favourite::query()->where('zoo_id', $id);

        if (!$favourite->exists()) {
            throw new FavouriteNotFoundException();
        }

        if (!$favourite->where('user_id', $userId)->exists()) {
            throw new UnauthorizedException();
        }

        return $favourite;
    }
}
