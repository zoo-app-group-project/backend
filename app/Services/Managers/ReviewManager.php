<?php

declare(strict_types=1);

namespace Zoo\Services\Managers;

use Illuminate\Database\Eloquent\Builder;
use Zoo\Exceptions\ReviewNotFoundException;
use Zoo\Exceptions\UnauthorizedException;
use Zoo\Exceptions\ZooAlreadyReviewed;
use Zoo\Exceptions\ZooNotFoundException;
use Zoo\Models\ZooReview;
use Zoo\Services\ZooRepository;

/**
 * Class ReviewManager
 * @package Zoo\Services\Managers
 */
class ReviewManager
{
    /** @var ZooRepository */
    protected $zooRepository;

    /**
     * ReviewManager constructor.
     * @param ZooRepository $zooRepository
     */
    public function __construct(ZooRepository $zooRepository)
    {
        $this->zooRepository = $zooRepository;
    }

    /**
     * @param array $data
     * @return ZooReview
     * @throws ZooAlreadyReviewed
     * @throws ZooNotFoundException
     */
    public function create(array $data): ZooReview
    {
        if (!$this->zooRepository->exists($data['zooId'])) {
            throw new ZooNotFoundException();
        }

        if ($this->zooRepository->isReviewedByUser($data['zooId'], $data['userId'])) {
            throw new ZooAlreadyReviewed();
        }

        return ZooReview::query()->create([
            'user_id' => $data['userId'],
            'zoo_id' => $data['zooId'],
            'rating' => $data['rating'],
            'content' => $data['content'],
        ]);
    }

    /**
     * @param array $data
     * @return ZooReview
     * @throws ReviewNotFoundException
     * @throws UnauthorizedException
     */
    public function update(array $data): ZooReview
    {
        $review = $this->getReviewQuery($data['id'], $data['userId'])->update([
            'rating' => $data['rating'],
            'content' => $data['content'],
        ]);

        return $this->getReviewQuery($data['id'], $data['userId'])->first();
    }

    /**
     * @param array $data
     * @throws ReviewNotFoundException
     * @throws UnauthorizedException
     */
    public function delete(array $data): void
    {
        $this->getReviewQuery($data['id'], $data['userId'])->delete();
    }

    /**
     * @param string $id
     * @param string $userId
     * @return Builder
     * @throws ReviewNotFoundException
     * @throws UnauthorizedException
     */
    protected function getReviewQuery(string $id, string $userId): Builder
    {
        $review = ZooReview::query()->where('id', $id);

        if (!$review->exists()) {
            throw new ReviewNotFoundException();
        }

        if (!$review->where('user_id', $userId)->exists()) {
            throw new UnauthorizedException();
        }

        return $review;
    }
}
