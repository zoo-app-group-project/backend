<?php

declare(strict_types=1);

namespace Zoo\Services;

use Zoo\Exceptions\ZooNotFoundException;
use Zoo\Interfaces\GeolocalizerClientInterface as GeolocalizerClient;
use Zoo\Models\Favourite;
use Zoo\Models\ZooReview;

/**
 * Class ZooRepository
 * @package Zoo\Services
 */
class ZooRepository
{
    /** @var GeolocalizerClient */
    protected $client;

    /**
     * ZooQuery constructor.
     * @param GeolocalizerClient $client
     */
    public function __construct(GeolocalizerClient $client)
    {
        $this->client = $client;
    }

    /**
     * @param string $id
     * @return bool
     */
    public function exists(string $id): bool
    {
        try {
            $this->client->get("/points/zoo/$id");
        } catch (ZooNotFoundException $exception) {
            return false;
        }

        return true;
    }

    /**
     * @param string $zooId
     * @param string $userId
     * @return bool
     */
    public function isReviewedByUser(string $zooId, string $userId): bool
    {
        return ZooReview::query()
            ->where([
                'zoo_id' => $zooId,
                'user_id' => $userId,
            ])
            ->exists();
    }

    /**
     * @param string $zooId
     * @param string $userId
     * @return bool
     */
    public function isFavouritedByUser(string $zooId, string $userId): bool
    {
        return Favourite::query()
            ->where([
                'zoo_id' => $zooId,
                'user_id' => $userId,
            ])
            ->exists();
    }
}
