@geocoding

Feature: Searching cities coordinates by city name and basic graphQL queries behaviour

  Background:
    Given initialized application

  Scenario: I want to find polish city by its name, to retrieve its name, full name and coordinates
    Given graphQL request:
    """
    query {
      findCities(query: "Legn" country: "pl") {
          name
          fullName
          location {
              longitude
              latitude
          }
      }
    }
    """
    When request is sent
    Then response should exist
    And response should not have any errors
    And response should have:
      | key                                  | value                                       |
      | data                                 |                                             |
      | data.findCities                      |                                             |
      | data.findCities.0                    |                                             |
      | data.findCities.0.name               | Legnica                                     |
      | data.findCities.0.fullName           | Legnica, Lower Silesian Voivodeship, Poland |
      | data.findCities.0.location           |                                             |
      | data.findCities.0.location.longitude | 16.1603187                                  |
      | data.findCities.0.location.latitude  | 51.2081617                                  |

  Scenario: I want to find city by its name, but I don't want to specify country because it should be optional attribute
    Given graphQL request:
    """
    query {
      findCities(query: "Legn") {
          name
          fullName
          location {
              longitude
              latitude
          }
      }
    }
    """
    When request is sent
    Then response should exist
    And response should not have any errors
    And response should have:
      | key                                  | value                                       |
      | data                                 |                                             |
      | data.findCities                      |                                             |
      | data.findCities.0                    |                                             |
      | data.findCities.0.name               | Legnica                                     |
      | data.findCities.0.fullName           | Legnica, Lower Silesian Voivodeship, Poland |
      | data.findCities.0.location           |                                             |
      | data.findCities.0.location.longitude | 16.1603187                                  |
      | data.findCities.0.location.latitude  | 51.2081617                                  |

  Scenario: I want to find city by its name, to retrieve only coordinates
    Given graphQL request:
    """
    query {
      findCities(query: "Legn") {
          location {
              longitude
              latitude
          }
      }
    }
    """
    When request is sent
    Then response should exist
    And response should not have any errors
    And response should have:
      | key                                  | value      |
      | data                                 |            |
      | data.findCities                      |            |
      | data.findCities.0                    |            |
      | data.findCities.0.location           |            |
      | data.findCities.0.location.longitude | 16.1603187 |
      | data.findCities.0.location.latitude  | 51.2081617 |
    And response should not have:
      | key                        |
      | data.findCities.0.name     |
      | data.findCities.0.fullName |

  Scenario: I want to find city by its name, to retrieve only full name
    Given graphQL request:
    """
    query {
      findCities(query: "Legn") {
          fullName
      }
    }
    """
    When request is sent
    Then response should exist
    And response should not have any errors
    And response should have:
      | key                        | value                                       |
      | data                       |                                             |
      | data.findCities            |                                             |
      | data.findCities.0          |                                             |
      | data.findCities.0.fullName | Legnica, Lower Silesian Voivodeship, Poland |
    And response should not have:
      | key                                  |
      | data.findCities.0.name               |
      | data.findCities.0.location.longitude |
      | data.findCities.0.location.latitude  |

  Scenario: I could try to not provide city name in query which is required parameter so I should receive proper validation error then
    Given graphQL request:
    """
    query {
      findCities(country: "ua") {
          name
          fullName
          location {
              longitude
              latitude
          }
      }
    }
    """
    When request is sent
    Then response should exist
    And response should have:
      | key              | value                                                                               |
      | errors           |                                                                                     |
      | errors.0.message | Field "findCities" argument "query" of type "String!" is required but not provided. |
    And "errors" should count "1" elements

  Scenario: I could try to provide city query of wrong type so I should receive proper validation error then
    Given graphQL request:
    """
    query {
      findCities(query: 1) {
          name
          fullName
          location {
              longitude
              latitude
          }
      }
    }
    """
    When request is sent
    Then response should exist
    And response should have:
      | key              | value                                                               |
      | errors           |                                                                     |
      | errors.0.message | Field "findCities" argument "query" requires type String!, found 1. |
    And "errors" should count "1" elements

  Scenario: I could try to query fields which are not defined in type definition of City so I should receive proper validation errors then
    Given graphQL request:
    """
    query {
      findCities(query: "Bogatynia") {
          namee
          crimeRate
          drugsSold
          formerMayorsInPrison
      }
    }
    """
    When request is sent
    Then response should exist
    And response should have:
      | key              | value                                                           |
      | errors           |                                                                 |
      | errors.0.message | Cannot query field "namee" on type "City". Did you mean "name"? |
      | errors.1.message | Cannot query field "crimeRate" on type "City".                  |
      | errors.2.message | Cannot query field "drugsSold" on type "City".                  |
      | errors.3.message | Cannot query field "formerMayorsInPrison" on type "City".       |
    And "errors" should count "4" elements

  Scenario: I could try use query as mutation so I should receive proper validation error then
    Given graphQL request:
    """
    mutation {
      findCities(query: "Legn") {
          fullName
      }
    }
    """
    When request is sent
    Then response should exist
    And response should have:
      | key              | value                                               |
      | errors           |                                                     |
      | errors.0.message | Cannot query field "findCities" on type "Mutation". |
    And "errors" should count "1" elements