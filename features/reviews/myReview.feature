@reviews

Feature: fetching review of currently authenticated user of specific zoo

  Background:
    Given initialized application
    And user with "test@example.com" email, "Test" name and "secret" password exists

  Scenario: As authenticated user I want to see my review of the zoo
    Given user with "test@example.com" email is authenticated
    And zoo with id "5df14a534f15997edd650ce7" is already reviewed by "test@example.com" with "5" rating and "Cool." content
    And graphQL request:
    """
    query {
      zoo(id: "5df14a534f15997edd650ce7") {
          myReview {
            id
            user {
                id
                name
                email
            }
            zooId
            rating
            content
            createdAt
          }
      }
    }
    """
    When request is sent
    Then response should exist
    And response should not have any errors
    And response should have:
      | key                          | value                    |
      | data                         |                          |
      | data.zoo                     |                          |
      | data.zoo.myReview            |                          |
      | data.zoo.myReview.id         |                          |
      | data.zoo.myReview.user       |                          |
      | data.zoo.myReview.user.id    |                          |
      | data.zoo.myReview.user.name  | Test                     |
      | data.zoo.myReview.user.email | test@example.com         |
      | data.zoo.myReview.zooId      | 5df14a534f15997edd650ce7 |
      | data.zoo.myReview.rating     | 5                        |
      | data.zoo.myReview.content    | Cool.                    |
      | data.zoo.myReview.createdAt  |                          |

  Scenario: As authenticated user who didn't posted any review I could try to fetch my review of the zoo so I should receive null then
    Given user with "test@example.com" email is authenticated
    And graphQL request:
    """
    query {
      zoo(id: "5df14a534f15997edd650ce7") {
          myReview {
            id
            user {
                id
                name
                email
            }
            zooId
            rating
            content
            createdAt
          }
      }
    }
    """
    When request is sent
    Then response should exist
    And response should not have any errors
    And response should have:
      | key               | value |
      | data              |       |
      | data.zoo          |       |
      | data.zoo.myReview |       |
    And response should not have:
      | key                          |
      | data.zoo.myReview.id         |
      | data.zoo.myReview.user       |
      | data.zoo.myReview.user.id    |
      | data.zoo.myReview.user.name  |
      | data.zoo.myReview.user.email |
      | data.zoo.myReview.zooId      |
      | data.zoo.myReview.rating     |
      | data.zoo.myReview.content    |
      | data.zoo.myReview.createdAt  |

  Scenario: As unauthenticated user, I could try to fetch my review of the zoo so I should receive null then
    And graphQL request:
    """
    query {
      zoo(id: "5df14a534f15997edd650ce7") {
          myReview {
            id
            user {
                id
                name
                email
            }
            zooId
            rating
            content
            createdAt
          }
      }
    }
    """
    When request is sent
    Then response should exist
    And response should not have any errors
    And response should have:
      | key               | value |
      | data              |       |
      | data.zoo          |       |
      | data.zoo.myReview |       |
    And response should not have:
      | key                          |
      | data.zoo.myReview.id         |
      | data.zoo.myReview.user       |
      | data.zoo.myReview.user.id    |
      | data.zoo.myReview.user.name  |
      | data.zoo.myReview.user.email |
      | data.zoo.myReview.zooId      |
      | data.zoo.myReview.rating     |
      | data.zoo.myReview.content    |
      | data.zoo.myReview.createdAt  |