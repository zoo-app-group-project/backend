@reviews

Feature: updating reviews

  Background:
    Given initialized application
    And user with "test@example.com" email, "Test" name and "secret" password exists
    And user with "test2@example.com" email, "Test2" name and "secret" password exists

  Scenario: As authenticated user, I want to update review of the zoo
    Given user with "test@example.com" email is authenticated
    And zoo with id "5df14a534f15997edd650ce7" is already reviewed by "test@example.com" with "5" rating and "Cool." content
    And graphQL request to update review posted by "test@example.com" of "5df14a534f15997edd650ce7" zoo
    When request is sent
    Then response should exist
    And response should not have any errors
    And response should have:
      | key                          | value                    |
      | data                         |                          |
      | data.updateReview            |                          |
      | data.updateReview.id         |                          |
      | data.updateReview.user       |                          |
      | data.updateReview.user.id    |                          |
      | data.updateReview.user.name  | Test                     |
      | data.updateReview.user.email | test@example.com         |
      | data.updateReview.zooId      | 5df14a534f15997edd650ce7 |
      | data.updateReview.rating     | 1                        |
      | data.updateReview.content    | Very bad.                |
      | data.updateReview.createdAt  |                          |
    And average rating of zoo with id "5df14a534f15997edd650ce7" should be "1"
    And review of zoo with id "5df14a534f15997edd650ce7" posted by "test@example.com" with "1" rating and "Very bad." content should exist in the database

  Scenario: As authenticated user, I could try to update non-existing review so I should receive proper error then
    Given user with "test@example.com" email is authenticated
    And graphQL request:
    """
     mutation {
        updateReview(
            id: "non-existing-review-uuid"
            rating: 5
            content: "Great!"
        ) {
            id
            user {
                id
                name
                email
            }
            zooId
            rating
            content
            createdAt
        }
    }
    """
    When request is sent
    Then response should exist
    And response should have:
      | key              | value             |
      | errors           |                   |
      | errors.0.message | Review not found. |
    And "errors" should count "1" elements

  Scenario: As unauthenticated user, I could try to update review so I should receive unauthenticated error then
    Given graphQL request:
    """
     mutation {
        updateReview(
            id: "some-review-uuid"
            rating: 5
            content: "Great!"
        ) {
            id
            user {
                id
                name
                email
            }
            zooId
            rating
            content
            createdAt
        }
    }
    """
    When request is sent
    Then response should exist
    And response should have:
      | key              | value             |
      | errors           |                   |
      | errors.0.message | Unauthenticated. |
    And "errors" should count "1" elements

  Scenario: As authenticated user, I could try to update someone else's review so I should receive unauthorized error then
    Given user with "test@example.com" email is authenticated
    And zoo with id "5df14a534f15997edd650ce7" is already reviewed by "test2@example.com" with "5" rating and "Cool." content
    And graphQL request to update review posted by "test2@example.com" of "5df14a534f15997edd650ce7" zoo
    When request is sent
    Then response should exist
    And response should have:
      | key              | value         |
      | errors           |               |
      | errors.0.message | Unauthorized. |
    And "errors" should count "1" elements
    And review of zoo with id "5df14a534f15997edd650ce7" posted by "test2@example.com" with "5" rating and "Cool." content should exist in the database