@reviews

Feature: Fetching average rating of specific zoo based on all users' reviews

  Background:
    Given initialized application
    And user with "test1@example.com" email, "Test1" name and "secret" password exists
    And user with "test2@example.com" email, "Test2" name and "secret" password exists
    And user with "test3@example.com" email, "Test3" name and "secret" password exists
    And user with "test4@example.com" email, "Test4" name and "secret" password exists
    And user with "test5@example.com" email, "Test5" name and "secret" password exists

  Scenario: I want to see average rating of given zoo reviewed only by no-one
    Given graphQL request:
    """
    query {
      zoo(id: "5df14a534f15997edd650ce7") {
        rating
      }
    }
    """
    When request is sent
    Then response should exist
    And response should not have any errors
    And response should have:
      | key             | value |
      | data            |       |
      | data.zoo        |       |
      | data.zoo        |       |
      | data.zoo.rating | 0     |

  Scenario Outline: I want to see average rating of given zoo reviewed only by 1 user
    Given zoo with id "5df14a534f15997edd650ce7" is already reviewed by "test1@example.com" with "<rating>" rating and "Some content." content
    Given graphQL request:
    """
    query {
      zoo(id: "5df14a534f15997edd650ce7") {
        rating
      }
    }
    """
    When request is sent
    Then response should exist
    And response should not have any errors
    And response should have:
      | key             | value |
      | data            |       |
      | data.zoo        |       |
      | data.zoo        |       |
      | data.zoo.rating | <avg> |

    Examples:
      | rating | avg |
      | 1      | 1   |
      | 2      | 2   |
      | 3      | 3   |
      | 4      | 4   |
      | 5      | 5   |

  Scenario Outline: I want to see average rating of given zoo reviewed 5 times by different users
    Given zoo with id "5df14a534f15997edd650ce7" is already reviewed by "test1@example.com" with "<rating_1>" rating and "Some content." content
    Given zoo with id "5df14a534f15997edd650ce7" is already reviewed by "test2@example.com" with "<rating_2>" rating and "Some content." content
    Given zoo with id "5df14a534f15997edd650ce7" is already reviewed by "test3@example.com" with "<rating_3>" rating and "Some content." content
    Given zoo with id "5df14a534f15997edd650ce7" is already reviewed by "test4@example.com" with "<rating_4>" rating and "Some content." content
    Given zoo with id "5df14a534f15997edd650ce7" is already reviewed by "test5@example.com" with "<rating_5>" rating and "Some content." content
    Given graphQL request:
    """
    query {
      zoo(id: "5df14a534f15997edd650ce7") {
        rating
      }
    }
    """
    When request is sent
    Then response should exist
    And response should not have any errors
    And response should have:
      | key             | value |
      | data            |       |
      | data.zoo        |       |
      | data.zoo        |       |
      | data.zoo.rating | <avg> |

    Examples:
      | rating_1 | rating_2 | rating_3 | rating_4 | rating_5 | avg |
      | 1        | 1        | 1        | 1        | 1        | 1   |
      | 5        | 5        | 5        | 5        | 5        | 5   |
      | 1        | 2        | 3        | 4        | 5        | 3   |
      | 1        | 1        | 5        | 5        | 5        | 3.4 |
      | 1        | 1        | 1        | 1        | 5        | 1.8 |

  Scenario Outline: I want to see average rating of given zoo reviewed 3 times by different users
    Given zoo with id "5df14a534f15997edd650ce7" is already reviewed by "test1@example.com" with "<rating_1>" rating and "Some content." content
    Given zoo with id "5df14a534f15997edd650ce7" is already reviewed by "test2@example.com" with "<rating_2>" rating and "Some content." content
    Given zoo with id "5df14a534f15997edd650ce7" is already reviewed by "test3@example.com" with "<rating_3>" rating and "Some content." content
    Given graphQL request:
    """
    query {
      zoo(id: "5df14a534f15997edd650ce7") {
        rating
      }
    }
    """
    When request is sent
    Then response should exist
    And response should not have any errors
    And response should have:
      | key             | value |
      | data            |       |
      | data.zoo        |       |
      | data.zoo        |       |
      | data.zoo.rating | <avg> |

    Examples:
      | rating_1 | rating_2 | rating_3 | avg  |
      | 1        | 1        | 1        | 1    |
      | 5        | 5        | 5        | 5    |
      | 1        | 2        | 3        | 2    |
      | 1        | 1        | 5        | 2.33 |
      | 5        | 4        | 1        | 3.33 |
      | 5        | 5        | 1        | 3.67 |
      | 5        | 4        | 2        | 3.67 |
