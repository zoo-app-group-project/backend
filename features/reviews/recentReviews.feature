@reviews

Feature: fetching review of currently authenticated user of specific zoo

  Background:
    Given initialized application
    And user with "test1@example.com" email, "Test1" name and "secret" password exists
    And user with "test2@example.com" email, "Test2" name and "secret" password exists

  Scenario: I want to see recent reviews of the zoo
    And zoo with id "5df14a534f15997edd650ce7" is already reviewed by "test1@example.com" with "5" rating and "Cool." content
    And zoo with id "5df14a534f15997edd650ce7" is already reviewed by "test2@example.com" with "1" rating and "Bad." content
    And graphQL request:
    """
    query {
      zoo(id: "5df14a534f15997edd650ce7") {
          recentReviews {
            user {
                name
                email
            }
            zooId
            rating
            content
          }
      }
    }
    """
    When request is sent
    Then response should exist
    And response should not have any errors
    And response should have:
      | key                                 | value                    |
      | data                                |                          |
      | data.zoo                            |                          |
      | data.zoo.recentReviews              |                          |
      | data.zoo.recentReviews.0            |                          |
      | data.zoo.recentReviews.0.user       |                          |
      | data.zoo.recentReviews.0.user.name  | Test1                    |
      | data.zoo.recentReviews.0.user.email | test1@example.com        |
      | data.zoo.recentReviews.0.zooId      | 5df14a534f15997edd650ce7 |
      | data.zoo.recentReviews.0.rating     | 5                        |
      | data.zoo.recentReviews.0.content    | Cool.                    |
      | data.zoo.recentReviews.1            |                          |
      | data.zoo.recentReviews.1.user       |                          |
      | data.zoo.recentReviews.1.user.name  | Test2                    |
      | data.zoo.recentReviews.1.user.email | test2@example.com        |
      | data.zoo.recentReviews.1.zooId      | 5df14a534f15997edd650ce7 |
      | data.zoo.recentReviews.1.rating     | 1                        |
      | data.zoo.recentReviews.1.content    | Bad.                     |

  Scenario: I could try to see recent reviews of the zoo which wasn't reviewed so I should receive empty array then
    And graphQL request:
    """
    query {
      zoo(id: "5df14a534f15997edd650ce7") {
          recentReviews {
            user {
                name
                email
            }
            zooId
            rating
            content
          }
      }
    }
    """
    When request is sent
    Then response should exist
    And response should not have any errors
    And response should have:
      | key                    | value |
      | data                   |       |
      | data.zoo               |       |
      | data.zoo.recentReviews |       |
    And response should not have:
      | key                                 |
      | data.zoo.recentReviews.0            |
      | data.zoo.recentReviews.0.user.name  |
      | data.zoo.recentReviews.0.user.email |
      | data.zoo.recentReviews.0.zooId      |
      | data.zoo.recentReviews.0.rating     |
      | data.zoo.recentReviews.0.content    |