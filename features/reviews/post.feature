@reviews

Feature: Posting reviews

  Background:
    Given initialized application
    And user with "test@example.com" email, "Test" name and "secret" password exists
    And user with "test2@example.com" email, "Test2" name and "secret" password exists

  Scenario Outline: As authenticated user, I want to post review of the zoo
    Given user with "test@example.com" email is authenticated
    And graphQL request:
    """
     mutation {
        postReview(
            zooId: "5df14a534f15997edd650ce7"
            rating: <rating>
            content: "<content>"
        ) {
            id
            user {
                id
                name
                email
            }
            zooId
            rating
            content
            createdAt
        }
    }
    """
    When request is sent
    Then response should exist
    And response should not have any errors
    And response should have:
      | key                        | value                    |
      | data                       |                          |
      | data.postReview            |                          |
      | data.postReview.id         |                          |
      | data.postReview.user       |                          |
      | data.postReview.user.id    |                          |
      | data.postReview.user.name  | Test                     |
      | data.postReview.user.email | test@example.com         |
      | data.postReview.zooId      | 5df14a534f15997edd650ce7 |
      | data.postReview.rating     | <rating>                 |
      | data.postReview.content    | <content>                |
      | data.postReview.createdAt  |                          |
    And average rating of zoo with id "5df14a534f15997edd650ce7" should be "<rating>"
    And review of zoo with id "5df14a534f15997edd650ce7" posted by "test@example.com" with "<rating>" rating and "<content>" content should exist in the database

    Examples:
      | rating | content           |
      | 5      | Great!            |
      | 4      | Good!             |
      | 3      | Not bad.          |
      | 2      | I've seen better. |
      | 1      | Awful.            |

  Scenario: As unauthenticated user, I could try to post review of the zoo so I should receive unauthenticated error then
    Given graphQL request:
    """
     mutation {
        postReview(
            zooId: "5df14a534f15997edd650ce7"
            rating: 1
            content: "Bad."
        ) {
            id
            rating
            content
        }
    }
    """
    When request is sent
    Then response should exist
    And response should have:
      | key              | value            |
      | errors           |                  |
      | errors.0.message | Unauthenticated. |
    And "errors" should count "1" elements

  Scenario: As authenticated user who already posted review of specific zoo, I could try to post review the same zoo so I should receive proper error then
    Given user with "test@example.com" email is authenticated
    And zoo with id "5df14a534f15997edd650ce7" is already reviewed by "test@example.com" with "5" rating and "Cool." content
    And graphQL request:
    """
     mutation {
        postReview(
            zooId: "5df14a534f15997edd650ce7"
            rating: 1
            content: "Bad."
        ) {
            id
            rating
            content
        }
    }
    """
    When request is sent
    Then response should exist
    And response should have:
      | key              | value                         |
      | errors           |                               |
      | errors.0.message | This zoo is already reviewed. |
    And "errors" should count "1" elements

  Scenario: As authenticated user, I want to post review of the zoo even if someone else posted review of the same zoo
    Given user with "test@example.com" email is authenticated
    And zoo with id "5df14a534f15997edd650ce7" is already reviewed by "test2@example.com" with "5" rating and "Cool." content
    And graphQL request:
    """
     mutation {
        postReview(
            zooId: "5df14a534f15997edd650ce7"
            rating: 1
            content: "Bad."
        ) {
            id
            rating
            content
        }
    }
    """
    When request is sent
    Then response should exist
    And response should not have any errors
    And response should have:
      | key                     | value |
      | data                    |       |
      | data.postReview         |       |
      | data.postReview.id      |       |
      | data.postReview.rating  | 1     |
      | data.postReview.content | Bad.  |
    And response should not have:
      | key                        |
      | data.postReview.user       |
      | data.postReview.user.id    |
      | data.postReview.user.name  |
      | data.postReview.user.email |
      | data.postReview.zooId      |
      | data.postReview.createdAt  |
    And average rating of zoo with id "5df14a534f15997edd650ce7" should be "3"

  Scenario: As authenticated user, I could try to post review of non-existing zoo so I should receive proper error then
    Given user with "test@example.com" email is authenticated
    And graphQL request:
    """
     mutation {
        postReview(
            zooId: "non-existing-zoo-uuid"
            rating: 1
            content: "Bad."
        ) {
            id
            rating
            content
        }
    }
    """
    When request is sent
    Then response should exist
    And response should have:
      | key              | value          |
      | errors           |                |
      | errors.0.message | Zoo not found. |
    And "errors" should count "1" elements

  Scenario: As authenticated user, I could try to post review without required attribute so I should receive proper validation error then
    Given user with "test@example.com" email is authenticated
    And graphQL request:
    """
     mutation {
        postReview(
            zooId: "5df14a534f15997edd650ce7"
            content: "Bad."
        ) {
            id
            rating
            content
        }
    }
    """
    When request is sent
    Then response should exist
    And response should have:
      | key              | value          |
      | errors           |                |
      | errors.0.message | Field "postReview" argument "rating" of type "Int!" is required but not provided. |
    And "errors" should count "1" elements