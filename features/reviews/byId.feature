@reviews

Feature: fetching single review by its id

  Background:
    Given initialized application
    And user with "test@example.com" email, "Test" name and "secret" password exists

  Scenario: I could try to fetch non existing review so I should receive null then
    Given zoo with id "5df14a534f15997edd650ce7" is already reviewed by "test@example.com" with "5" rating and "Cool." content
    Given graphQL request to fetch review posted by "test@example.com" of "5df14a534f15997edd650ce7" zoo
    When request is sent
    Then response should exist
    And response should not have any errors
    And response should have:
      | key                   | value                    |
      | data                  |                          |
      | data.review           |                          |
      | data.review.id        |                          |
      | data.review.user      |                          |
      | data.review.zooId     | 5df14a534f15997edd650ce7 |
      | data.review.rating    | 5                        |
      | data.review.content   | Cool.                    |
      | data.review.createdAt |                          |

  Scenario: I could try to fetch non existing review so I should receive null then
    Given graphQL request:
    """
    query {
      review(id: "non-existing-review-uuid") {
         id
         user {
           id
           name
           email
         }
         zooId
         rating
         content
         createdAt
      }
    }
    """
    When request is sent
    Then response should exist
    And response should not have any errors
    And response should have:
      | key         | value |
      | data        |       |
      | data.review |       |
    But response should not have:
      | key                   |
      | data.review.id        |
      | data.review.user      |
      | data.review.zooId     |
      | data.review.rating    |
      | data.review.content   |
      | data.review.createdAt |