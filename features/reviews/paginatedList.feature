@reviews

Feature: fetching paginated list of reviews of the zoo

  Background:
    Given initialized application
    And user with "test1@example.com" email, "Test1" name and "secret" password exists
    And user with "test2@example.com" email, "Test2" name and "secret" password exists
    And user with "test3@example.com" email, "Test3" name and "secret" password exists
    And user with "test4@example.com" email, "Test4" name and "secret" password exists
    And user with "test5@example.com" email, "Test5" name and "secret" password exists
    And zoo with id "5df14a534f15997edd650ce7" is already reviewed by "test1@example.com" with "1" rating and "review1" content
    And zoo with id "5df14a534f15997edd650ce7" is already reviewed by "test2@example.com" with "2" rating and "review2" content
    And zoo with id "5df14a534f15997edd650ce7" is already reviewed by "test3@example.com" with "3" rating and "review3" content
    And zoo with id "5df14a534f15997edd650ce7" is already reviewed by "test4@example.com" with "4" rating and "review4" content
    And zoo with id "5df14a534f15997edd650ce7" is already reviewed by "test5@example.com" with "5" rating and "review5" content

  Scenario: I could try to fetch first page of paginated list of reviews
    Given graphQL request:
    """
    query {
      reviews(
          zooId: "5df14a534f15997edd650ce7"
          orderBy: [
              {
                  field: "created_at",
                  order: DESC

              }
          ]
          first: 3
          page: 1
      )
      {
          data {
            rating
            content
          }
          paginatorInfo {
                currentPage
                lastPage
          }
      }
    }
    """
    When request is sent
    Then response should exist
    And response should not have any errors
    And response should have:
      | key                                    | value   |
      | data                                   |         |
      | data.reviews                           |         |
      | data.reviews.data.0.rating             | 1       |
      | data.reviews.data.0.content            | review1 |
      | data.reviews.paginatorInfo             |         |
      | data.reviews.paginatorInfo.currentPage | 1       |
      | data.reviews.paginatorInfo.lastPage    | 2       |
    And "data.reviews.data" should count "3" elements

  Scenario: I could try to fetch second page of paginated list of reviews
    Given graphQL request:
    """
    query {
      reviews(
          zooId: "5df14a534f15997edd650ce7"
          orderBy: [
              {
                  field: "created_at",
                  order: DESC

              }
          ]
          first: 3
          page: 2
      )
      {
          data {
            rating
            content
          }
          paginatorInfo {
                currentPage
                lastPage
          }
      }
    }
    """
    When request is sent
    Then response should exist
    And response should not have any errors
    And response should have:
      | key                                    | value   |
      | data                                   |         |
      | data.reviews                           |         |
      | data.reviews.data.0.rating             | 4       |
      | data.reviews.data.0.content            | review4 |
      | data.reviews.paginatorInfo             |         |
      | data.reviews.paginatorInfo.currentPage | 2       |
      | data.reviews.paginatorInfo.lastPage    | 2       |
    And "data.reviews.data" should count "2" elements
