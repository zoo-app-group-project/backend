@zoos

Feature: retrieving list of zoos in specific country

  Background:
    Given initialized application

  Scenario: I want to see list of zoos in Poland to see all available fields of Zoo type.
    Given graphQL request:
    """
    query {
      findZoosInCountry(countryCode: "pl") {
        id
        name
        location {
            longitude
            latitude
        }
        description
        logo
        wikipediaLink
        address {
          full
          city
          state
          country
          countryCode
        }
        myReview {
          id
          user {
              id
              name
              email
          }
          zooId
          rating
          content
          createdAt
        }
        recentReviews {
          id
          user {
              id
              name
              email
          }
          zooId
          rating
          content
          createdAt
        }
        rating
      }
    }
    """
    When request is sent
    Then response should exist
    And response should not have any errors
    And response should have:
      | key                                          | value                                                                                                                                                                                                                                                                                       |
      | data                                         |                                                                                                                                                                                                                                                                                             |
      | data.findZoosInCountry                       |                                                                                                                                                                                                                                                                                             |
      | data.findZoosInCountry.0                     |                                                                                                                                                                                                                                                                                             |
      | data.findZoosInCountry.0.id                  | 5df14a534f15997edd650ce7                                                                                                                                                                                                                                                                    |
      | data.findZoosInCountry.0.name                | Kraków Zoo                                                                                                                                                                                                                                                                                  |
      | data.findZoosInCountry.0.location            |                                                                                                                                                                                                                                                                                             |
      | data.findZoosInCountry.0.location.longitude  | 19.85056                                                                                                                                                                                                                                                                                    |
      | data.findZoosInCountry.0.location.latitude   | 50.05361                                                                                                                                                                                                                                                                                    |
      | data.findZoosInCountry.0.description         | The Kraków Zoo (Polish: Ogród Zoologiczny w Krakowie) is located in Kraków, Poland and was established in 1929. It is home to over 1500 animals and about 260 species. The zoo is a member of the European Association of Zoos and Aquaria and the World Association of Zoos and Aquariums. |
      | data.findZoosInCountry.0.logo                |                                                                                                                                                                                                                                                                                             |
      | data.findZoosInCountry.0.wikipediaLink       | https://en.wikipedia.org/wiki/Krak%C3%B3w_Zoo                                                                                                                                                                                                                                               |
      | data.findZoosInCountry.0.address             |                                                                                                                                                                                                                                                                                             |
      | data.findZoosInCountry.0.address.full        | Zoo Kraków, Aleja Żubrowa, Las Wolski, Zwierzyniec, Krakow, Lesser Poland Voivodeship, 30-232, Poland                                                                                                                                                                                       |
      | data.findZoosInCountry.0.address.city        | Krakow                                                                                                                                                                                                                                                                                      |
      | data.findZoosInCountry.0.address.state       | Lesser Poland Voivodeship                                                                                                                                                                                                                                                                   |
      | data.findZoosInCountry.0.address.country     | Poland                                                                                                                                                                                                                                                                                      |
      | data.findZoosInCountry.0.address.countryCode | pl                                                                                                                                                                                                                                                                                          |
      | data.findZoosInCountry.0.myReview            |                                                                                                                                                                                                                                                                                             |
      | data.findZoosInCountry.0.recentReviews       |                                                                                                                                                                                                                                                                                             |
      | data.findZoosInCountry.0.rating              | 0                                                                                                                                                                                                                                                                                           |
    And "data.findZoosInCountry" should count "7" elements
    And "data.findZoosInCountry.0.recentReviews" should count "0" elements

  Scenario: I want to see list of zoos in Poland to see their names and coordinates.
    Given graphQL request:
    """
    query {
      findZoosInCountry(countryCode: "pl") {
          name
          location {
              longitude
              latitude
          }
      }
    }
    """
    When request is sent
    Then response should exist
    And response should not have any errors
    And response should have:
      | key                                         | value      |
      | data                                        |            |
      | data.findZoosInCountry                      |            |
      | data.findZoosInCountry.0                    |            |
      | data.findZoosInCountry.0.name               | Kraków Zoo |
      | data.findZoosInCountry.0.location           |            |
      | data.findZoosInCountry.0.location.longitude | 19.85056   |
      | data.findZoosInCountry.0.location.latitude  | 50.05361   |
    And "data.findZoosInCountry" should count "7" elements
    And response should not have:
      | key                                          |
      | data.findZoosInCountry.0.description         |
      | data.findZoosInCountry.0.logo                |
      | data.findZoosInCountry.0.wikipediaLink       |
      | data.findZoosInCountry.0.address             |
      | data.findZoosInCountry.0.address.full        |
      | data.findZoosInCountry.0.address.city        |
      | data.findZoosInCountry.0.address.state       |
      | data.findZoosInCountry.0.address.country     |
      | data.findZoosInCountry.0.address.countryCode |
      | data.findZoosInCountry.0.myReview            |
      | data.findZoosInCountry.0.recentReviews       |
      | data.findZoosInCountry.0.rating              |
