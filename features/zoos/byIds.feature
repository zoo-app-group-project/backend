@zoos

Feature: retrieving list of zoos with given ids

  Background:
    Given initialized application

  Scenario: I want to query list of zoos with given ids to see all available fields of Zoo type.
    Given graphQL request:
    """
    query {
      findZoosByIds(ids: ["5df149ec22e2d721147a8585", "5df149ec4f15997edd650ba5"]) {
        id
        name
        location {
            longitude
            latitude
        }
        description
        logo
        wikipediaLink
        address {
          full
          city
          state
          country
          countryCode
        }
        myReview {
          id
          user {
              id
              name
              email
          }
          zooId
          rating
          content
          createdAt
        }
        recentReviews {
          id
          user {
              id
              name
              email
          }
          zooId
          rating
          content
          createdAt
        }
        rating
      }
    }
    """
    When request is sent
    Then response should exist
    And response should not have any errors
    And response should have:
      | key                                   | value                                                                                                                                                                                                                                                                                                          |
      | data                                  |                                                                                                                                                                                                                                                                                                                |
      | data.findZoosByIds                       |                                                                                                                                                                                                                                                                                                                |
      | data.findZoosByIds.0                     |                                                                                                                                                                                                                                                                                                                |
      | data.findZoosByIds.0.id                  | 5df149ec22e2d721147a8585                                                                                                                                                                                                                                                                                       |
      | data.findZoosByIds.0.name                | Rani Bagh, Hyderabad                                                                                                                                                                                                                                                                                           |
      | data.findZoosByIds.0.location            |                                                                                                                                                                                                                                                                                                                |
      | data.findZoosByIds.0.location.longitude  | 68.34305                                                                                                                                                                                                                                                                                                       |
      | data.findZoosByIds.0.location.latitude   | 25.38223                                                                                                                                                                                                                                                                                                       |
      | data.findZoosByIds.0.description         | The Rani Bagh ("Queen's Garden"), previously Das Garden, is a zoological garden located in Hyderabad City, Sindh, Pakistan. The garden was re-christened in honour of Queen Victoria. It was established as a botanical garden in 1861 by the then Agro-horticultural Society and later animals were moved in. |
      | data.findZoosByIds.0.logo                |                                                                                                                                                                                                                                                                                                                |
      | data.findZoosByIds.0.wikipediaLink       | https://en.wikipedia.org/wiki/Rani_Bagh,_Hyderabad                                                                                                                                                                                                                                                             |
      | data.findZoosByIds.0.address             |                                                                                                                                                                                                                                                                                                                |
      | data.findZoosByIds.0.address.full        | Thandi Sarak, Eidgah Colony, Hyderabad, , Sindh, 7100, Pakistan                                                                                                                                                                                                                                                |
      | data.findZoosByIds.0.address.city        | Hyderabad                                                                                                                                                                                                                                                                                                      |
      | data.findZoosByIds.0.address.state       | Sindh                                                                                                                                                                                                                                                                                                          |
      | data.findZoosByIds.0.address.country     | Pakistan                                                                                                                                                                                                                                                                                                       |
      | data.findZoosByIds.0.address.countryCode | pk                                                                                                                                                                                                                                                                                                             |
      | data.findZoosByIds.0.myReview            |                                                                                                                                                                                                                                                                                                                |
      | data.findZoosByIds.0.recentReviews       |                                                                                                                                                                                                                                                                                                                |
      | data.findZoosByIds.0.rating              | 0                                                                                                                                                                                                                                                                                                              |
    And "data.findZoosByIds" should count "9" elements
    And "data.findZoosByIds.0.recentReviews" should count "0" elements

  Scenario: I want to query list of zoos with given ids to see their names and coordinates.
    Given graphQL request:
    """
    query {
      findZoosByIds(ids: ["5df149ec22e2d721147a8585", "5df149ec4f15997edd650ba5"]) {
          name
          location {
              longitude
              latitude
          }
      }
    }
    """
    When request is sent
    Then response should exist
    And response should not have any errors
    And response should have:
      | key                                  | value                |
      | data                                 |                      |
      | data.findZoosByIds                      |                      |
      | data.findZoosByIds.0                    |                      |
      | data.findZoosByIds.0.name               | Rani Bagh, Hyderabad |
      | data.findZoosByIds.0.location           |                      |
      | data.findZoosByIds.0.location.longitude | 68.34305             |
      | data.findZoosByIds.0.location.latitude  | 25.38223             |
    And "data.findZoosByIds" should count "9" elements
    And response should not have:
      | key                                   |
      | data.findZoosByIds.0.description         |
      | data.findZoosByIds.0.logo                |
      | data.findZoosByIds.0.wikipediaLink       |
      | data.findZoosByIds.0.address             |
      | data.findZoosByIds.0.address.full        |
      | data.findZoosByIds.0.address.city        |
      | data.findZoosByIds.0.address.state       |
      | data.findZoosByIds.0.address.country     |
      | data.findZoosByIds.0.address.countryCode |
      | data.findZoosByIds.0.myReview            |
      | data.findZoosByIds.0.recentReviews       |
      | data.findZoosByIds.0.rating              |
