@zoos

Feature: searching zoos by name

  Background:
    Given initialized application

  Scenario: I want to find zoo by its name to see all available fields of Zoo type.
    Given graphQL request:
    """
    query {
      findZoosByName(query: "Zamosc") {
        id
        name
        location {
            longitude
            latitude
        }
        description
        logo
        wikipediaLink
        address {
          full
          city
          state
          country
          countryCode
        }
        myReview {
          id
          user {
              id
              name
              email
          }
          zooId
          rating
          content
          createdAt
        }
        recentReviews {
          id
          user {
              id
              name
              email
          }
          zooId
          rating
          content
          createdAt
        }
        rating
      }
    }
    """
    When request is sent
    Then response should exist
    And response should not have any errors
    And response should have:
      | key                                       | value                                                                                                                                                                                                                                                                                                                                         |
      | data                                      |                                                                                                                                                                                                                                                                                                                                               |
      | data.findZoosByName                       |                                                                                                                                                                                                                                                                                                                                               |
      | data.findZoosByName.0                     |                                                                                                                                                                                                                                                                                                                                               |
      | data.findZoosByName.0.id                  | 5df14a5322e2d721147a86c9                                                                                                                                                                                                                                                                                                                      |
      | data.findZoosByName.0.name                | Zamość Zoo                                                                                                                                                                                                                                                                                                                                    |
      | data.findZoosByName.0.location            |                                                                                                                                                                                                                                                                                                                                               |
      | data.findZoosByName.0.location.longitude  | 23.23917                                                                                                                                                                                                                                                                                                                                      |
      | data.findZoosByName.0.location.latitude   | 50.716694                                                                                                                                                                                                                                                                                                                                     |
      | data.findZoosByName.0.description         | The Zamość Zoo (Polish: Ogród Zoologiczny w Zamościu) is a zoological garden located in the city of Zamość, Lublin Voivodeship in Poland. It was established in 1918 and currently contains 2524 animals and 312 species covering the area of approximately 13 hectares. The zoo is a member of the European Association of Zoos and Aquaria. |
      | data.findZoosByName.0.logo                |                                                                                                                                                                                                                                                                                                                                               |
      | data.findZoosByName.0.wikipediaLink       | https://en.wikipedia.org/wiki/Zamo%C5%9B%C4%87_Zoo                                                                                                                                                                                                                                                                                            |
      | data.findZoosByName.0.address             |                                                                                                                                                                                                                                                                                                                                               |
      | data.findZoosByName.0.address.full        | ZOO Zamość, 12, Szczebrzeska, Osiedle Karolówka, Zamość, Lublin Voivodeship, 22-400, Poland                                                                                                                                                                                                                                                   |
      | data.findZoosByName.0.address.city        | Zamość                                                                                                                                                                                                                                                                                                                                        |
      | data.findZoosByName.0.address.state       | Lublin Voivodeship                                                                                                                                                                                                                                                                                                                            |
      | data.findZoosByName.0.address.country     | Poland                                                                                                                                                                                                                                                                                                                                        |
      | data.findZoosByName.0.address.countryCode | pl                                                                                                                                                                                                                                                                                                                                            |
      | data.findZoosByName.0.myReview            |                                                                                                                                                                                                                                                                                                                                               |
      | data.findZoosByName.0.recentReviews       |                                                                                                                                                                                                                                                                                                                                               |
      | data.findZoosByName.0.rating              | 0                                                                                                                                                                                                                                                                                                                                             |
    And "data.findZoosByName" should count "1" elements
    And "data.findZoosByName.0.recentReviews" should count "0" elements

  Scenario: I want to find zoo by its name to see their names and coordinates.
    Given graphQL request:
    """
    query {
      findZoosByName(query: "Zamosc") {
          name
          location {
              longitude
              latitude
          }
      }
    }
    """
    When request is sent
    Then response should exist
    And response should not have any errors
    And response should have:
      | key                                      | value      |
      | data                                     |            |
      | data.findZoosByName                      |            |
      | data.findZoosByName.0                    |            |
      | data.findZoosByName.0.name               | Zamość Zoo |
      | data.findZoosByName.0.location           |            |
      | data.findZoosByName.0.location.longitude | 23.23917   |
      | data.findZoosByName.0.location.latitude  | 50.716694  |
    And "data.findZoosByName" should count "1" elements
    And response should not have:
      | key                                       |
      | data.findZoosByName.0.description         |
      | data.findZoosByName.0.logo                |
      | data.findZoosByName.0.wikipediaLink       |
      | data.findZoosByName.0.address             |
      | data.findZoosByName.0.address.full        |
      | data.findZoosByName.0.address.city        |
      | data.findZoosByName.0.address.state       |
      | data.findZoosByName.0.address.country     |
      | data.findZoosByName.0.address.countryCode |
      | data.findZoosByName.0.myReview            |
      | data.findZoosByName.0.recentReviews       |
      | data.findZoosByName.0.rating              |
