@zoos

Feature: retrieving zoo by its id

  Background:
    Given initialized application

  Scenario: I want to find zoo by its id to see all available fields of Zoo type.
    Given graphQL request:
    """
    query {
      zoo(id: "5df14a534f15997edd650ce7") {
        id
        name
        location {
            longitude
            latitude
        }
        description
        logo
        wikipediaLink
        address {
          full
          city
          state
          country
          countryCode
        }
        myReview {
          id
          user {
              id
              name
              email
          }
          zooId
          rating
          content
          createdAt
        }
        recentReviews {
          id
          user {
              id
              name
              email
          }
          zooId
          rating
          content
          createdAt
        }
        rating
      }
    }
    """
    When request is sent
    Then response should exist
    And response should not have any errors
    And response should have:
      | key                          | value                                                                                                                                                                                                                                                                                       |
      | data                         |                                                                                                                                                                                                                                                                                             |
      | data.zoo                     |                                                                                                                                                                                                                                                                                             |
      | data.zoo                     |                                                                                                                                                                                                                                                                                             |
      | data.zoo.id                  | 5df14a534f15997edd650ce7                                                                                                                                                                                                                                                                    |
      | data.zoo.name                | Kraków Zoo                                                                                                                                                                                                                                                                                  |
      | data.zoo.location            |                                                                                                                                                                                                                                                                                             |
      | data.zoo.location.longitude  | 19.85056                                                                                                                                                                                                                                                                                    |
      | data.zoo.location.latitude   | 50.05361                                                                                                                                                                                                                                                                                    |
      | data.zoo.description         | The Kraków Zoo (Polish: Ogród Zoologiczny w Krakowie) is located in Kraków, Poland and was established in 1929. It is home to over 1500 animals and about 260 species. The zoo is a member of the European Association of Zoos and Aquaria and the World Association of Zoos and Aquariums. |
      | data.zoo.logo                |                                                                                                                                                                                                                                                                                             |
      | data.zoo.wikipediaLink       | https://en.wikipedia.org/wiki/Krak%C3%B3w_Zoo                                                                                                                                                                                                                                               |
      | data.zoo.address             |                                                                                                                                                                                                                                                                                             |
      | data.zoo.address.full        | Zoo Kraków, Aleja Żubrowa, Las Wolski, Zwierzyniec, Krakow, Lesser Poland Voivodeship, 30-232, Poland                                                                                                                                                                                       |
      | data.zoo.address.city        | Krakow                                                                                                                                                                                                                                                                                      |
      | data.zoo.address.state       | Lesser Poland Voivodeship                                                                                                                                                                                                                                                                   |
      | data.zoo.address.country     | Poland                                                                                                                                                                                                                                                                                      |
      | data.zoo.address.countryCode | pl                                                                                                                                                                                                                                                                                          |
      | data.zoo.myReview            |                                                                                                                                                                                                                                                                                             |
      | data.zoo.recentReviews       |                                                                                                                                                                                                                                                                                             |
      | data.zoo.rating              | 0                                                                                                                                                                                                                                                                                           |
    And "data.zoo.recentReviews" should count "0" elements

  Scenario: I want to find zoo by its id to see their names and coordinates.
    Given graphQL request:
    """
    query {
      zoo(id: "5df14a534f15997edd650ce7") {
          name
          location {
              longitude
              latitude
          }
      }
    }
    """
    When request is sent
    Then response should exist
    And response should not have any errors
    And response should have:
      | key                         | value      |
      | data                        |            |
      | data.zoo                    |            |
      | data.zoo                    |            |
      | data.zoo.name               | Kraków Zoo |
      | data.zoo.location           |            |
      | data.zoo.location.longitude | 19.85056   |
      | data.zoo.location.latitude  | 50.05361   |
    And response should not have:
      | key                          |
      | data.zoo.description         |
      | data.zoo.logo                |
      | data.zoo.wikipediaLink       |
      | data.zoo.address             |
      | data.zoo.address.full        |
      | data.zoo.address.city        |
      | data.zoo.address.state       |
      | data.zoo.address.country     |
      | data.zoo.address.countryCode |
      | data.zoo.myReview            |
      | data.zoo.recentReviews       |
      | data.zoo.rating              |
