@zoos

Feature: retrieving list of all zoos

  Background:
    Given initialized application

  Scenario: I want to query list of all zoos to see all available fields of Zoo type.
    Given graphQL request:
    """
    query {
      getAllZoos {
        id
        name
        location {
            longitude
            latitude
        }
        description
        logo
        wikipediaLink
        address {
          full
          city
          state
          country
          countryCode
        }
        myReview {
          id
          user {
              id
              name
              email
          }
          zooId
          rating
          content
          createdAt
        }
        recentReviews {
          id
          user {
              id
              name
              email
          }
          zooId
          rating
          content
          createdAt
        }
        rating
      }
    }
    """
    When request is sent
    Then response should exist
    And response should not have any errors
    And response should have:
      | key                                   | value                                                                                                                                                                                                                                                                                                          |
      | data                                  |                                                                                                                                                                                                                                                                                                                |
      | data.getAllZoos                       |                                                                                                                                                                                                                                                                                                                |
      | data.getAllZoos.0                     |                                                                                                                                                                                                                                                                                                                |
      | data.getAllZoos.0.id                  | 5df149ec22e2d721147a8585                                                                                                                                                                                                                                                                                       |
      | data.getAllZoos.0.name                | Rani Bagh, Hyderabad                                                                                                                                                                                                                                                                                           |
      | data.getAllZoos.0.location            |                                                                                                                                                                                                                                                                                                                |
      | data.getAllZoos.0.location.longitude  | 68.34305                                                                                                                                                                                                                                                                                                       |
      | data.getAllZoos.0.location.latitude   | 25.38223                                                                                                                                                                                                                                                                                                       |
      | data.getAllZoos.0.description         | The Rani Bagh ("Queen's Garden"), previously Das Garden, is a zoological garden located in Hyderabad City, Sindh, Pakistan. The garden was re-christened in honour of Queen Victoria. It was established as a botanical garden in 1861 by the then Agro-horticultural Society and later animals were moved in. |
      | data.getAllZoos.0.logo                |                                                                                                                                                                                                                                                                                                                |
      | data.getAllZoos.0.wikipediaLink       | https://en.wikipedia.org/wiki/Rani_Bagh,_Hyderabad                                                                                                                                                                                                                                                             |
      | data.getAllZoos.0.address             |                                                                                                                                                                                                                                                                                                                |
      | data.getAllZoos.0.address.full        | Thandi Sarak, Eidgah Colony, Hyderabad, , Sindh, 7100, Pakistan                                                                                                                                                                                                                                                |
      | data.getAllZoos.0.address.city        | Hyderabad                                                                                                                                                                                                                                                                                                      |
      | data.getAllZoos.0.address.state       | Sindh                                                                                                                                                                                                                                                                                                          |
      | data.getAllZoos.0.address.country     | Pakistan                                                                                                                                                                                                                                                                                                       |
      | data.getAllZoos.0.address.countryCode | pk                                                                                                                                                                                                                                                                                                             |
      | data.getAllZoos.0.myReview            |                                                                                                                                                                                                                                                                                                                |
      | data.getAllZoos.0.recentReviews       |                                                                                                                                                                                                                                                                                                                |
      | data.getAllZoos.0.rating              | 0                                                                                                                                                                                                                                                                                                              |
    And "data.getAllZoos" should count "9" elements
    And "data.getAllZoos.0.recentReviews" should count "0" elements

  Scenario: I want to query list of all zoos to see their names and coordinates.
    Given graphQL request:
    """
    query {
      getAllZoos {
          name
          location {
              longitude
              latitude
          }
      }
    }
    """
    When request is sent
    Then response should exist
    And response should not have any errors
    And response should have:
      | key                                  | value                |
      | data                                 |                      |
      | data.getAllZoos                      |                      |
      | data.getAllZoos.0                    |                      |
      | data.getAllZoos.0.name               | Rani Bagh, Hyderabad |
      | data.getAllZoos.0.location           |                      |
      | data.getAllZoos.0.location.longitude | 68.34305             |
      | data.getAllZoos.0.location.latitude  | 25.38223             |
    And "data.getAllZoos" should count "9" elements
    And response should not have:
      | key                                   |
      | data.getAllZoos.0.description         |
      | data.getAllZoos.0.logo                |
      | data.getAllZoos.0.wikipediaLink       |
      | data.getAllZoos.0.address             |
      | data.getAllZoos.0.address.full        |
      | data.getAllZoos.0.address.city        |
      | data.getAllZoos.0.address.state       |
      | data.getAllZoos.0.address.country     |
      | data.getAllZoos.0.address.countryCode |
      | data.getAllZoos.0.myReview            |
      | data.getAllZoos.0.recentReviews       |
      | data.getAllZoos.0.rating              |
