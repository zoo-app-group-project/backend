@zoo

Feature: retrieving list of nearby zoos

  Background:
    Given initialized application

  Scenario: I want to query list of nearby zoos to see all available fields of Zoo type.
    Given graphQL request:
    """
    query {
      findNearbyZoos(longitude: 16.1603187, latitude: 51.2081617, range: 100) {
        id
        name
        location {
            longitude
            latitude
        }
        description
        logo
        wikipediaLink
        address {
          full
          city
          state
          country
          countryCode
        }
        myReview {
          id
          user {
              id
              name
              email
          }
          zooId
          rating
          content
          createdAt
        }
        recentReviews {
          id
          user {
              id
              name
              email
          }
          zooId
          rating
          content
          createdAt
        }
        rating
      }
    }
    """
    When request is sent
    Then response should exist
    And response should not have any errors
    And response should have:
      | key                                       | value                                                                                           |
      | data                                      |                                                                                                 |
      | data.findNearbyZoos                       |                                                                                                 |
      | data.findNearbyZoos.0                     |                                                                                                 |
      | data.findNearbyZoos.0.id                  | 5df14a704f15997edd650d44                                                                        |
      | data.findNearbyZoos.0.name                | Görlitz Zoo                                                                                     |
      | data.findNearbyZoos.0.location            |                                                                                                 |
      | data.findNearbyZoos.0.location.longitude  | 14.98028                                                                                        |
      | data.findNearbyZoos.0.location.latitude   | 51.14167                                                                                        |
      | data.findNearbyZoos.0.description         | The Görlitz Zoo (German: Naturschutz-Tierpark Görlitz) is a zoo in Görlitz, in Saxony, Germany. |
      | data.findNearbyZoos.0.logo                |                                                                                                 |
      | data.findNearbyZoos.0.wikipediaLink       | https://en.wikipedia.org/wiki/G%C3%B6rlitz_Zoo                                                  |
      | data.findNearbyZoos.0.address             |                                                                                                 |
      | data.findNearbyZoos.0.address.full        | Tierpark Görlitz, 43, Zittauer Straße, Südstadt, Görlitz, Saxony, 02826, Germany                |
      | data.findNearbyZoos.0.address.city        | Görlitz                                                                                         |
      | data.findNearbyZoos.0.address.state       | Saxony                                                                                          |
      | data.findNearbyZoos.0.address.country     | Germany                                                                                         |
      | data.findNearbyZoos.0.address.countryCode | de                                                                                              |
      | data.findNearbyZoos.0.myReview            |                                                                                                 |
      | data.findNearbyZoos.0.recentReviews       |                                                                                                 |
      | data.findNearbyZoos.0.rating              | 0                                                                                               |
    And "data.findNearbyZoos" should count "7" elements
    And "data.findNearbyZoos.0.recentReviews" should count "0" elements

  Scenario: I want to query list of nearby zoos to see their names and coordinates.
    Given graphQL request:
    """
    query {
      findNearbyZoos(longitude: 16.1603187, latitude: 51.2081617, range: 100) {
          name
          location {
              longitude
              latitude
          }
      }
    }
    """
    When request is sent
    Then response should exist
    And response should not have any errors
    And response should have:
      | key                                      | value       |
      | data                                     |             |
      | data.findNearbyZoos                      |             |
      | data.findNearbyZoos.0                    |             |
      | data.findNearbyZoos.0.name               | Görlitz Zoo |
      | data.findNearbyZoos.0.location           |             |
      | data.findNearbyZoos.0.location.longitude | 14.98028    |
      | data.findNearbyZoos.0.location.latitude  | 51.14167    |
    And "data.findNearbyZoos" should count "7" elements
    And response should not have:
      | key                                       |
      | data.findNearbyZoos.0.description         |
      | data.findNearbyZoos.0.logo                |
      | data.findNearbyZoos.0.wikipediaLink       |
      | data.findNearbyZoos.0.address             |
      | data.findNearbyZoos.0.address.full        |
      | data.findNearbyZoos.0.address.city        |
      | data.findNearbyZoos.0.address.state       |
      | data.findNearbyZoos.0.address.country     |
      | data.findNearbyZoos.0.address.countryCode |
      | data.findNearbyZoos.0.myReview            |
      | data.findNearbyZoos.0.recentReviews       |
      | data.findNearbyZoos.0.rating              |
