@auth

Feature: Creating and authenticating users

  Background:
    Given initialized application

  Scenario: As unauthenticated user, I want to create new user
    Given graphQL request:
    """
    mutation {
      createUser(
         email: "test@example.com"
         name: "Test Testowy"
         password: "secret"
      ) {
          id
          name
          email
      }
    }
    """
    When request is sent
    Then response should exist
    And response should not have any errors
    And response should have:
      | key                   | value            |
      | data                  |                  |
      | data.createUser       |                  |
      | data.createUser.id    |                  |
      | data.createUser.name  | Test Testowy     |
      | data.createUser.email | test@example.com |

  Scenario: While creating new user, I could provide wrong email format so I should receive validation error then
    Given graphQL request:
    """
    mutation {
      createUser(
         email: "test"
         name: "Test Testowy"
         password: "secret"
      ) {
          id
          name
          email
      }
    }
    """
    When request is sent
    Then response should exist
    And response should have:
      | key                                    | value                                         |
      | errors                                 |                                               |
      | errors.0.message                       | Validation failed for the field [createUser]. |
      | errors.0.extensions.validation.email.0 | The email must be a valid email address.      |
    And "errors" should count "1" elements


  Scenario: While creating new user, I could provide already taken email so I should receive validation error then
    Given user with "test@example.com" email, "Test" name and "secret" password exists
    And graphQL request:
    """
    mutation {
      createUser(
         email: "test@example.com"
         name: "Test Testowy"
         password: "secret"
      ) {
          id
          name
          email
      }
    }
    """
    When request is sent
    Then response should exist
    And response should have:
      | key                                    | value                                         |
      | errors                                 |                                               |
      | errors.0.message                       | Validation failed for the field [createUser]. |
      | errors.0.extensions.validation.email.0 | The email has already been taken.             |
    And "errors" should count "1" elements

  Scenario: As unauthenticated user, I want to login to be able to receive access token
    Given user with "test@example.com" email, "Test" name and "secret" password exists
    And graphQL request:
    """
    mutation {
      issueToken(
          email: "test@example.com"
          password: "secret"
        )
    }
    """
    When request is sent
    Then response should exist
    And response should not have any errors
    And response should have:
      | key             |
      | data            |
      | data.issueToken |

  Scenario: While logging in, I could provide wrong credentials so I should receive proper error then
    Given user with "test@example.com" email, "Test" name and "secret" password exists
    And graphQL request:
    """
    mutation {
      issueToken(
          email: "test@example.com"
          password: "wrongPassword"
        )
    }
    """
    When request is sent
    Then response should exist
    And response should have:
      | key              | value                |
      | errors           |                      |
      | errors.0.message | Invalid credentials. |
    And "errors" should count "1" elements
