@friends

Feature: friend system

    Background:
        Given initialized application
        And user with "test@example.com" email, "Test" name and "secret" password exists
        And user with "test2@example.com" email, "Test2" name and "secret" password exists

    Scenario: As authenticated user, I want to add other user as friend
        Given user with "test@example.com" email is authenticated
        And graphQL request:
        """
        mutation {
            addFriend(
                email: "test2@example.com"
            )
            {
                id
                name
                email
            }
        }
        """
        When request is sent
        Then response should exist
        And response should not have any errors
        And response should have:
            | key                  | value             |
            | data                 |                   |
            | data.addFriend       |                   |
            | data.addFriend.id    |                   |
            | data.addFriend.name  | Test2             |
            | data.addFriend.email | test2@example.com |
        And user "test@example.com" should have "1" friends

    Scenario: As unauthenticated user, I want to add other user as friend and should receive error then
        Given graphQL request:
        """
        mutation {
            addFriend(
                email: "test2@example.com"
            )
            {
                id
                name
                email
            }
        }
        """
        When request is sent
        Then response should exist
        And response should have:
            | key              | value            |
            | errors           |                  |
            | errors.0.message | Unauthenticated. |
        And "errors" should count "1" elements

    Scenario: As authenticated user, I want to add not existing user as friend
        Given user with "test@example.com" email is authenticated
        And graphQL request:
        """
        mutation {
            addFriend(
               email: "non-existing-email@example.com"
            )
            {
                id
                name
                email
            }
        }
        """
        When request is sent
        Then response should exist
        And response should have:
            | key              | value           |
            | errors           |                 |
            | errors.0.message | User not found. |
        And "errors" should count "1" elements

    Scenario: As authenticated user, I want to add myself as a friend and receive error then
        Given user with "test@example.com" email is authenticated
        And graphQL request:
        """
        mutation {
            addFriend(
               email: "test@example.com"
            )
            {
                id
                name
                email
            }
        }
        """
        When request is sent
        Then response should exist
        And response should have:
            | key              | value                          |
            | errors           |                                |
            | errors.0.message | Cannot add yourself as friend. |
        And "errors" should count "1" elements

    Scenario: As authenticated user, I want to remove friend
        Given user with "test@example.com" email is authenticated
        And user "test@example.com" has friend with email "test2@example.com"
        And graphQL request:
        """
        mutation {
            removeFriend(
               email: "test2@example.com"
            )
        }
        """
        When request is sent
        Then response should exist
        And response should have:
            | key               | value |
            | data              |       |
            | data.removeFriend |       |
        And user "test@example.com" should have "0" friends

    Scenario: As authenticated user, I want to retrieve my friend list
        Given user with "test@example.com" email is authenticated
        And user "test@example.com" has friend with email "test2@example.com"
        And graphQL request:
        """
        query{
            friendList
            {
                id
                name
                email
            }
        }
         """
        When request is sent
        Then response should exist
        And response should not have any errors
        And response should have:
            | key                     | value             |
            | data                    |                   |
            | data.friendList         |                   |
            | data.friendList.0.id    |                   |
            | data.friendList.0.name  | Test2             |
            | data.friendList.0.email | test2@example.com |
        And "data.friendList" should count "1" elements

    Scenario: As authenticated user, I want to get my friend favourite zoos
        Given user with "test@example.com" email is authenticated
        And user "test@example.com" has friend with email "test2@example.com"
        And zoo with id "5df149ec22e2d721147a8585" is already added to favourites by user "test2@example.com"
        And graphQL request:
        """
        query{
            friendFavourites(email: "test2@example.com")
            {
                id
                user {
                    id
                    name
                    email
                }
                zooId
                createdAt
            }
        }
        """
        When request is sent
        Then response should exist
        And response should not have any errors
        And response should have:
            | key                                | value                    |
            | data                               |                          |
            | data.friendFavourites              |                          |
            | data.friendFavourites.0            |                          |
            | data.friendFavourites.0.id         |                          |
            | data.friendFavourites.0.user       |                          |
            | data.friendFavourites.0.user.email | test2@example.com        |
            | data.friendFavourites.0.zooId      | 5df149ec22e2d721147a8585 |
            | data.friendFavourites.0.createdAt  |                          |
        And "data.friendFavourites" should count "1" elements

    Scenario: As authenticated user, I want to get favourite zoos of not a friend
        Given user with "test@example.com" email is authenticated
        And zoo with id "5df149ec22e2d721147a8585" is already added to favourites by user "test2@example.com"
        And graphQL request:
        """
        query{
            friendFavourites(email: "test2@example.com")
            {
                id
                user {
                    id
                    name
                    email
                }
                zooId
                createdAt
            }
        }
        """
        When request is sent
        Then response should exist
        And response should have:
            | key              | value                                 |
            | errors           |                                       |
            | errors.0.message | Cannot access favourites of stranger. |
        And "errors" should count "1" elements
