@favourites

Feature: Adding and retrieving favourite zoos

    Background:
        Given initialized application
        And user with "test@example.com" email, "Test" name and "secret" password exists

    Scenario: As authenticated user, I want to add zoo to favourites
        Given user with "test@example.com" email is authenticated
        And graphQL request to add zoo with id "5df14a534f15997edd650ce7" to favourites
        When request is sent
        Then response should exist
        And response should not have any errors
        And response should have:
            | key                          | value                    |
            | data                         |                          |
            | data.addFavourite            |                          |
            | data.addFavourite.user       |                          |
            | data.addFavourite.user.id    |                          |
            | data.addFavourite.user.name  | Test                     |
            | data.addFavourite.user.email | test@example.com         |
            | data.addFavourite.zooId      | 5df14a534f15997edd650ce7 |
            | data.addFavourite.createdAt  |                          |
        And user "test@example.com" should have "1" favourite zoos

    Scenario: As unauthenticated user, I want to add zoo to favourites and I should receive unauthenticated error then
        Given graphQL request to add zoo with id "5df14a534f15997edd650ce7" to favourites
        When request is sent
        Then response should exist
        And response should have:
            | key              | value            |
            | errors           |                  |
            | errors.0.message | Unauthenticated. |
        And "errors" should count "1" elements

    Scenario: As authenticated user, I want to remove zoo from favourites
        Given user with "test@example.com" email is authenticated
        And zoo with id "5df14a534f15997edd650ce7" is already added to favourites by user "test@example.com"
        And graphQL request:
        """
        mutation {
            removeFavourite(zooId: "5df14a534f15997edd650ce7")
        }
        """
        When request is sent
        Then response should exist
        And response should have:
            | key                  | value |
            | data                 |       |
            | data.removeFavourite |       |
        And user "test@example.com" should have "0" favourite zoos

    Scenario: As authenticated user, I want to retrieve list of my favourites
        Given user with "test@example.com" email is authenticated
        And zoo with id "5df149ec22e2d721147a8585" is already added to favourites by user "test@example.com"
        And graphQL request:
            """
            query {
                myFavourites {
                    id
                    user {
                        id
                        name
                        email
                    }
                    zooId
                    createdAt
                }
            }
            """
        When request is sent
        Then response should exist
        And response should not have any errors
        And response should have:
            | key                            | value                    |
            | data                           |                          |
            | data.myFavourites              |                          |
            | data.myFavourites.0            |                          |
            | data.myFavourites.0.id         |                          |
            | data.myFavourites.0.user       |                          |
            | data.myFavourites.0.user.email | test@example.com         |
            | data.myFavourites.0.zooId      | 5df149ec22e2d721147a8585 |
            | data.myFavourites.0.createdAt  |                          |
        And "data.myFavourites" should count "1" elements
