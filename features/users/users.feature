@users

Feature: Retrieving information about users

  Background:
    Given initialized application
    And user with "test@example.com" email, "Test" name and "secret" password exists
    And user with "test2@example.com" email, "Test2" name and "secret" password exists

  Scenario: As unauthenticated user, I want to retrieve specific user id, name and email
    Given graphQL query request for "test2@example.com" user
    When request is sent
    Then response should exist
    And response should not have any errors
    And response should have:
      | key             | value             |
      | data            |                   |
      | data.user       |                   |
      | data.user.id    |                   |
      | data.user.name  | Test2             |
      | data.user.email | test2@example.com |

  Scenario: As unauthenticated user, I could try to retrieve non-existing user so I should receive null user then
    Given graphQL request:
    """
    query {
      user(id: "non-existing-user-uuid") {
          id
          name
          email
      }
    }
    """
    When request is sent
    Then response should exist
    And response should not have any errors
    And response should have:
      | key       |
      | data      |
      | data.user |
    And "data.user" should be set but null
    And response should not have:
      | key             |
      | data.user.id    |
      | data.user.name  |
      | data.user.email |

  Scenario: As authenticated user, I want to retrieve users ids, names and emails
    Given user with "test@example.com" email is authenticated
    Given graphQL request:
    """
    query {
      users {
          id
          name
          email
      }
    }
    """
    When request is sent
    Then response should exist
    And response should not have any errors
    And response should have:
      | key                | value            |
      | data               |                  |
      | data.users         |                  |
      | data.users.0.id    |                  |
      | data.users.0.name  | Test             |
      | data.users.0.email | test@example.com |
    And "data.users" should count "2" elements

  Scenario: As authenticated user, I want to retrieve users emails
    Given user with "test@example.com" email is authenticated
    Given graphQL request:
    """
    query {
      users {
          email
      }
    }
    """
    When request is sent
    Then response should exist
    And response should not have any errors
    And response should have:
      | key                |
      | data.users.0.email |
    And response should not have:
      | key               |
      | data.users.0.id   |
      | data.users.0.name |
    And "data.users" should count "2" elements

  Scenario: As unauthenticated user, I could try to retrieve all users so I should receive unauthenticated error then
    Given graphQL request:
    """
    query {
      users {
          id
          name
          email
      }
    }
    """
    When request is sent
    Then response should exist
    And response should have:
      | key              | value            |
      | errors           |                  |
      | errors.0.message | Unauthenticated. |
    And "errors" should count "1" elements
    And response should not have:
      | key  |
      | data |

  Scenario: As authenticated user, I want to retrieve my account information
    Given user with "test@example.com" email is authenticated
    Given graphQL request:
    """
    query {
      me {
          id
          name
          email
      }
    }
    """
    When request is sent
    Then response should exist
    And response should not have any errors
    And response should have:
      | key           | value            |
      | data          |                  |
      | data.me       |                  |
      | data.me.id    |                  |
      | data.me.name  | Test             |
      | data.me.email | test@example.com |

  Scenario: As unauthenticated user, I could try to retrieve my account information so I should receive Unauthenticated error then
    Given graphQL request:
    """
    query {
      me {
          id
          name
          email
      }
    }
    """
    When request is sent
    Then response should exist
    And response should have:
      | key              | value            |
      | errors           |                  |
      | errors.0.message | Unauthenticated. |
    And "errors" should count "1" elements
    And response should not have:
      | key  |
      | data |