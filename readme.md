### Instrukcja uruchomienia aplikacji w środowisku deweloperskim

#### 1. Stwórz plik .env

```
cp .env.example .env
```

#### 2. Ustaw hasło do bazy danych w pliku .env
np.
```
DB_PASSWORD=test123
```

#### 3. Utwórz kontenery i uruchom je

```
docker-compose create
docker-compose up
```


#### 4. Uruchamiaj komendy z poziomu kontenera z zainstalowanym PHP

```
docker container exec -it zoo-php-fpm /bin/bash
cd /application
```

#### 5. Instalacja zależności
```
composer install
```

#### 6. Wygeneruj APP_KEY

```
php artisan key:generate
```

#### 7. Migracje i seedy

```
php artisan migrate --seed
```

#### 8. Konfiguracja serwera OAuth Passport

```
php artisan passport:install
```

Skopiuj `secret` drugiego klienta z pary wygenerowanych klientów do pliku `.env`:

```
OAUTH_CLIENT_SECRET=tutaj_wklej_secret
```

#### Aplikacja powinna być dostępna pod adresem: `http://localhost/graphql`